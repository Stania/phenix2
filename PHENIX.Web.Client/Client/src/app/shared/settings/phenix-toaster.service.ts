import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, Request } from '@angular/http';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

@Injectable()
export class PhenixToasterService {
  private toasterService: ToasterService;

  constructor(private toastSrv: ToasterService) {
    this.toasterService = toastSrv;
  }

  showError(titre, message) {
    this.toasterService.pop('error', titre, message);
  }
  showInfo(titre, message) {
    this.toasterService.pop('info', titre, message);
  }
  showSuccess(titre, message) {
    this.toasterService.pop('success', titre, message);
  }
  showWarning(titre, message) {
    this.toasterService.pop('warning', titre, message);
  }
}
