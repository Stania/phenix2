import {FormGroup} from '@angular/forms';
import { FormControl } from '@angular/forms/src/model';

export class ValidationService {

// export static function getValidatorErrorMessage(code: string) {
//   let config = {
//       required : 'Ce champ est obligatoire',
//       invalidEmailAddress: 'Adresse mail invalide',
//       invalidPassword: 'Le mot de passe doit contenir au minimum 8 caractères avec au moins une majuscule, une miniscule et un chiffre.',
//       invalidDebutPeriod : 'La date de début doit être inférieur à la date de fin',
//       invalidFinPeriod : 'La date de fin doit être supérieur à la date de début'
//   };
//   return config[code];
// }


    static emailValidator(control) {

        if (control.value && control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { invalidEmailAddress: true };
        }
    }



    static confirmPassword(form: FormGroup, field) {
      let fieldChanges = false;
      return function innerFunction(control) {
        if (!fieldChanges) {
          form.get(field).valueChanges
            .subscribe(() => {
              control.updateValueAndValidity();
            });
          fieldChanges = true;
        }
        if (control.value === form.get(field).value) {
          return null;
        } else {
          return { not_matching: true };
        }
      };
    }
    static periodValidator(control : FormControl,form : FormGroup, aSurveiller : string,isDebutPeriode : boolean) {
      if(control.value == null)
      return null;
      let date = form.get(aSurveiller);
        if (date != null) {
          if(date.value == null)
          return null;
          if (date.valid == false)
            date.updateValueAndValidity();
          if(isDebutPeriode){
            return control.value >= date.value ?  { invalidDebutPeriod: true }: null;
          }else{
            return control.value <= date.value ? { invalidFinPeriod: true } : null;
          }
        }
      }

    static passwordValidator(control) {
        if (control.value === null || control.value === '') return null;
        if (control.value && control.value.match(/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(.{8,100})$/)) {
            return null;
        } else {
            return { invalidPassword: true };
        }
    }
}
