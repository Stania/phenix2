import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AutreDepeche, AutreDepecheType } from "../../models/DFV/dfv.models";

@Injectable()
export class AutreDepecheService {
    baseUrl: string = this.urlService.getUrlApi() + "api/AutreDepeche/";

    constructor(private urlService: UrlService, private http: HttpClient) {
    }

    updateAutreDepeche(autreDepeche : AutreDepeche):Observable<AutreDepeche> {
      const url = this.baseUrl + "update/";
      return this.http.post<AutreDepeche>(url,autreDepeche,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    createAutreDepecheManoeuvre(autreDepeche : AutreDepeche):Observable<AutreDepeche>  {
      const url = this.baseUrl + "createAutreDepecheManoeuvre/";
      return this.http.post<AutreDepeche>(url,autreDepeche,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    createAutreDepecheDFV(autreDepeche : AutreDepeche):Observable<AutreDepeche>  {
      const url = this.baseUrl + "createAutreDepecheDFV/";
      return this.http.post<AutreDepeche>(url,autreDepeche,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    getAllAutreDepecheByIdDFV(id: Number) :Observable<Array<AutreDepeche>> {
      let url = this.baseUrl +"GetAllAutreDepecheByIdDFV/" + id;
      return this.http.post<any>(url,null,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     })
    }


    public deleteAutreDepeche(id: Number) :Observable<any> {
      let url = this.baseUrl +"DeleteAutreDepeche/" + id;
      return this.http.post<any>(url,null,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     })
    }
}
