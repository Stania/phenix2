import { AutreDepeche } from "../DFV/dfv.models";

export class ChoixAutreDepecheVM{
  public ChoixAutreDepeche : number;
  public AutreDepeche : AutreDepeche;
}
