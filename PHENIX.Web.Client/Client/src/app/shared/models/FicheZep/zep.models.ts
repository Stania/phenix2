import { EntityBase } from "../Commun/entityBase.model";
import { Manoeuvre, DFV, MesureProtectionPrise, VerificationZepLibre_IPCS, VerificationZepLibre_Ligne, Aiguille_DFV } from "../DFV/dfv.models";
import { Secteur, UserProfil, ProfilPoste } from "../Profils/profil.models";

export class Aiguille extends EntityBase {
  public Nom: string;
  public ZepId: number;
  public Zep: Zep;
  public Aiguille_DFVList: Array<Aiguille_DFV>;
}

export class CompositionZep extends EntityBase {
  public ZepEnfantId: number;
  public ZepEnfant: Zep;
  public GroupeZepId: number;
  public GroupeZep: Zep;
}

export class IncompatibiliteZep extends EntityBase {
  public ZepId: number;
  public Zep: Zep;
  public ZepIncompatibleId: number;
  public ZepIncompatible: Zep;
}

export class MesureProtection_Zep extends EntityBase {
  public Valeur: string;
  public ZepId: number;
  public PosteId: number;
  public Zep: Zep;
  public Poste: Poste;
  public MesureProtectionTypeId: number;
  public MesureProtectionType: MesureProtectionType;
}

export class MesureProtectionType extends EntityBase {
  public Nom: string;
  public MesureProtection_ZepList: Array<MesureProtection_Zep>;
}

export class ObjetGraphique extends EntityBase {
  public Coordonnees: string;
  public Type: string;
  public Color: string;
  public ZepId: number;
  public Zep: Zep;
}

export class Particularite extends EntityBase {
  public Valeur: string;
  public ZepId: number;
  public Zep: Zep;
  public ParticulariteTypeId: number;
  public ParticulariteType: ParticulariteType;
}

export class ParticulariteType extends EntityBase {
  public Nom: string;
  public ParticulariteList: Array<Particularite>;
}

export class PointRemarquable_Zep extends EntityBase {
  public Valeur: string;
  public ZepId: number;
  public Zep: Zep;
  public PosteConcerneId: number;
  public PosteConcerne: Poste;
  public PointRemarquableTypeId: number;
  public PointRemarquableType: PointRemarquableType;
  public ManoeuvreList: Array<Manoeuvre>;
}

export class PointRemarquableType extends EntityBase {
  public Nom: string;
  public PointRemarquable_ZepList: Array<PointRemarquable_Zep>;
}

export class Poste extends EntityBase {
  public Nom: string;
  public SecteurId: number;
  public Secteur: Secteur;
  public ZepList: Array<Zep>;
  public MesureProtection_ZepList: Array<MesureProtection_Zep>;
  public PointRemarquable_ZepList: Array<PointRemarquable_Zep>;
  public DFVList: Array<DFV>;
  public MesureProtectionPriseList: Array<MesureProtectionPrise>;
  public VerificationZepLibre_IPCS: VerificationZepLibre_IPCS;
  public VerificationZepLibre_Ligne: VerificationZepLibre_Ligne;
  public VerificationList: Array<Verification>;
  public UserProfilList: Array<UserProfil>;
  public ProfilPosteList: Array<ProfilPoste>;
}

export class Verification extends EntityBase {
  public Nom: string;
  public PosteId: number;
  public Poste: Poste;
  public ZepId: number;
  public Zep: Zep;
  public VerificationTypeId: number;
  public VerificationType: VerificationType;
}

export class VerificationType extends EntityBase {
  public Nom: string;
  public VerificationList: Array<Verification>;
}

export class Zep extends EntityBase {
  public Numero: string;
  public ZepTypeId: number;
  public ZepType: ZepType;
  public Indice: string;
  public DateDebutApplicabilite: Date;
  public DateFinApplicabilite: Date;
  public FichierImage: string;
  public PosteDemandeDFV: Poste;
  public PosteDemandeDFVId: number | null;
  public AutorisationTTX: boolean | null;
  public AutorisationLAM: boolean | null;
  public TTxStationne: string;
  public AutresDispositionsParticulieres: string;
  public DFVAvecVerificationLiberation: boolean | null;
  public DFVSansVerificationLiberationDerriereTrainOuvrant: boolean | null;
  public DFVSansVerificationLiberationTTxDeclencheur: boolean | null;
  public DFVSansVerificationLiberationTTxStationne: boolean | null;
  public DFVRestitueeOccupee: boolean | null;
  public DFVSansVerificationLiberationTTxStationnePartieG: boolean | null;
  public GeqSansTTx: boolean | null;
  public GeqAvecTTx: boolean | null;
  public CompositionZepEnfantList: Array<CompositionZep>;
  public CompositionGroupeZepList: Array<CompositionZep>;
  public IncompatibiliteZepList: Array<IncompatibiliteZep>;
  public IncompatibiliteAutresZepList: Array<IncompatibiliteZep>;
  public MesureProtection_ZepList: Array<MesureProtection_Zep>;
  public PointRemarquable_ZepList: Array<PointRemarquable_Zep>;
  public ParticulariteList: Array<Particularite>;
  public AiguilleList: Array<Aiguille>;
  public DFVList: Array<DFV>;
  public VerificationList: Array<Verification>;
  public ObjetGraphiqueList: Array<ObjetGraphique>;
}

export class ZepType extends EntityBase {
  public Nom: string;
  public ZepList: Array<Zep>;
}







