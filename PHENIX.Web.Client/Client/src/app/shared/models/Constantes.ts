export class Constantes{
  static readonly TypesObjets =
  {
      Ligne : "Ligne",
      DashedLigne : "DashedLigne",
      Extremite : "Extremite",
      SautDeMouton : "SautDeMouton"
  };
  static readonly colors = ["red", "orange", "yellow", "green", "blue", "cyan", "purple"];

  static readonly MAX = 120;
  static readonly MIN = 1;
}

//utilisation
var unType = Constantes.TypesObjets.Extremite;
var max = Constantes.MAX;
var min = Constantes.MIN;

export class MesureProtectionTypes
{
  static readonly ProtectionEtVerification = "Conditions de protection et vérifications des postes";
  static readonly ItinérairesAdetruirePRCI = "Itinéraires à détruire(PRCI)";
  static readonly ItinérairesAdetruireEtAMunirDunDA = "Itinéraires à détruire et à munir de DA";
  static readonly OrganeDeCommandeAplacerEtAmunirDeDA = "Organe de commande à placer dans la position indiquée et munis d'un DA";
  static readonly DialogueDeProtection = "Dialogue de protection";
  static readonly AutorisationADetruire = "Autorisation à détruire";
  static readonly AiguilleImmobiliseeDansPositionIndiquee = "Aiguille immobilisée dans la position indiquée";
  static readonly JalonDarretAplacerSurLeTerrain = "Jalon d'arrêt à placer sur le terrain";
}

export class ParticulariteTypes
{
  //Applicable dans les conditions(Poste ouvert / Poste fermé)
  static readonly ApplicableDansCondition = "Applicable dans les conditions(Poste ouvert / Poste fermé)";
  //Dispositions particulières
  static readonly DFVResititueeOccupee = "DFV restituée occupée";
  static readonly TTXStationne = "TTX Stationné";
  static readonly AutresDispositionsParticulieresTTX = "Autre dispositions particulières TTX";
  //Procédés DFV
  static readonly AiguillesAPositionObligee = "Aiguilles à position obligée";
  static readonly AiguillesADisposerDansPositionPrevue = "Aiguilles à disposer dans la position prévue avant accord";
  static readonly DispositionsParticulieresParExploitation = "Dispositions particulières par l'Exploitation";
  //Mesure à prendre par l'équipements
  static readonly OutilDeBouclageParLEquipement = "Outil de bouclage par l'Equipement";
  static readonly DispositionsParticulieresParLEquipement = "Dispositions particulières par l'Equipement";
  static readonly AutresMesuresEventiellesParLEquipement = "Autres mesures éventielles par l'Equipement";
}

export class ZepTypes
{
  static readonly TypeL = "Type L";
  static readonly TypeG = "Type G";
  static readonly TypeLG = "Type L + G";
}

export class PointRemarquableTypes
{
  static readonly PointEngagement = "Point d'engagement";
  static readonly PointDegagement = "Point de dégagement";
  static readonly SignauxIntermediaires = "Signaux intermédiaires";
}

export class ManoeuvreTypes
{
  static readonly Engagement = "Engagement";
  static readonly Degagement = "Dégagement";
  static readonly Franchissement = "Franchissement";
}

export class OccupationTypes
{
  static readonly OccupeeALAccord = "ZEP occupée par TTx à l'accord de la DFV";
  static readonly OccupeeALaRestitution = "ZEP occupée par TTx à la restitution";
}

export class Statuts
{
  static readonly EnDemande = "En création";
  static readonly  EnAccord = "En attente d'accord";
  static readonly  Accordee = "Accordée";
  static readonly  Restituee = "Restituée";
  static readonly  Annulee = "Annulée";
  static readonly  Refusee = "Refusée";
}

export class Secteurs
{
  static readonly Mantes = "Mantes";
}
