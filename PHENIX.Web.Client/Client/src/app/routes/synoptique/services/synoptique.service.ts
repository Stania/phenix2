import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/map';
//import { Zep } from '../../../models/zep/zep';
const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable()
export class SynoptiqueService {
  ZepsUrl: string;

  constructor(private http : HttpClient) {
    this.ZepsUrl = "api/zeps";
  }

  // getZeps (): Observable<Zep[]> {
  //   return this.http.get<Zep[]>(this.ZepsUrl)
  //     //.do(data => console.log(data)) // eyeball results in the console
  //     .catch(this.handleError);
  // }

  // addZep(inputZep : Zep):Observable<boolean>{
  //   //console.log(inputZep.Numero);
  //   //const zep : Zep = {Numero:inputZep.numero, objets : inputZep.};
  //   return this.http.post<Zep>(this.ZepsUrl, inputZep, cudOptions)
  //   .catch(this.handleError);
  // }

  // deleteZep (zep: Zep | number): Observable<Zep> {
  //   const id = typeof zep === 'number' ? zep : zep.id;
  //   const url = `${this.ZepsUrl}/${id}`;

  //   return this.http.delete<Zep>(url, cudOptions)
  //     .catch(this.handleError);
  // }

  private handleError (error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    // and reformat for user consumption
    console.error(error); // log to console instead
    return Observable.throw(error);
  }



}
