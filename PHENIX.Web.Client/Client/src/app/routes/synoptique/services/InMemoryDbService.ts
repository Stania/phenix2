import {InMemoryDbService, RequestInfo} from 'angular-in-memory-web-api';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

@Injectable()
export class InMemoryZepsService implements InMemoryDbService{
    createDb(reqInfo?: RequestInfo) {
        let zeps = [
          { id: 1, numero: '1',color : '',  objets : [] },
        ];

        let GroupesZeps = [
          {id:1,numero:'101-102', zeps : [1,2]}
        ]

        let dfv = [
          {id:1, numero :1, dateDebut : '12/12/2017', idZep : 1},
          {id:2, numero :2, dateDebut : '12/12/2017', idZep : 2}
        ]

        // default returnType
        // let returnType  = 'object';
        let returnType  = 'observable';
        // let returnType  = 'promise';

        // demonstrate POST commands/resetDb
        // this example clears the collections if the request body tells it to do so
        if (reqInfo) {
            const body = reqInfo.utils.getJsonBody(reqInfo.req) || {};
            if (body.clear === true) {
                zeps.length = 0;
            }
        }

        // 'returnType` can be 'object' | 'observable' | 'promise'
        //returnType = body.returnType || 'object';

        const db = { zeps };

        switch (returnType) {
            case ('observable'):
              return of(db).delay(10);
            case ('promise'):
              return new Promise(resolve => {
                setTimeout(() => resolve(db), 10);
              });
            default:
              return db;
        }
    }
}
