import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {SynoptiqueService} from  '../../services/synoptique.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/Subject';
import { ObjetGraphiqueVM} from '../../../../shared/models/ViewModels/ObjetGraphiqueVM';
import { ZepService } from '../../../../shared/services/ficheZep/zep.service';
import { ObjetGraphique, Zep } from '../../../../shared/models/FicheZep/zep.models';
import { PhenixToasterService } from '../../../../shared/settings/phenix-toaster.service';


@Component({
  selector: 'link-zep',
  templateUrl: './link-zep.component.html',
  styleUrls: ['./link-zep.component.scss']
})
export class LinkZepComponent  implements OnInit {
  objetsGraphiquesIHMList : Array<ObjetGraphiqueVM>;
  objetsGraphiquesList : Array<ObjetGraphique>;
  listZep : Array<Zep>;
  selectedZep : Zep;
  selectedColor : string;

  public onClose: Subject<Zep>;

  constructor(public modal: BsModalRef, private synoService: SynoptiqueService, private zepService : ZepService,
    private toastSrv : PhenixToasterService
  ) {
    this.objetsGraphiquesIHMList = new Array<ObjetGraphiqueVM>();
    this.onClose = new Subject();
    this.listZep = new Array<Zep>();
    this.selectedZep = new Zep();
  }
  ngOnInit() {

    const result =  this.zepService.getNotDrawedZepList();
    result.subscribe(
      data => {
        this.listZep = data;
     },
     error => {
      this.toastSrv.showError("Erreur",error.error.error);
     });
  }

  linkZEP() {
    this.selectedZep.ObjetGraphiqueList = new Array<ObjetGraphique>();
    this.objetsGraphiquesIHMList.forEach(element => {
      element.color = this.selectedColor;
      var obj = element.toObjetGraphique();
      obj.ZepId = this.selectedZep.Id;
      this.selectedZep.ObjetGraphiqueList.push(obj);
    });
    const result =  this.zepService.linkDrawZep(this.selectedZep);
      result.subscribe(
        data => {
          this.close(data);
       },
       error => {
        this.toastSrv.showError("Erreur",error.error.error);
       });

  }

  close(item: Zep) {
    this.onClose.next(item);
    this.modal.hide();
  }

}
