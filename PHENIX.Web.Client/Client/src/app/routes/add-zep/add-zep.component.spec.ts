import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddZepComponent } from './add-zep.component';

describe('AddZepComponent', () => {
  let component: AddZepComponent;
  let fixture: ComponentFixture<AddZepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddZepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddZepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
