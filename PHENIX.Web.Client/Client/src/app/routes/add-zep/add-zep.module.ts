import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddZepComponent } from './add-zep.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

const routes : Routes = [
  {path:'', component: AddZepComponent}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule

  ],
  declarations: [AddZepComponent],
  exports:[RouterModule],
  entryComponents:[AddZepComponent]
})
export class AddZepModule { }
