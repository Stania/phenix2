import { Component, OnInit } from '@angular/core';
import { ZepService } from '../../shared/services/ficheZep/zep.service';
import { Observable, Subject } from "rxjs/Rx";
import { PhenixToasterService } from "../../shared/settings/phenix-toaster.service";
import { Zep, ZepType, MesureProtection_Zep, Poste, PointRemarquable_Zep } from "../../shared/models/FicheZep/zep.models";
import { MesuresService } from '../../shared/services/ficheDfv/mesures.service';
import { PointRemarquableService } from '../../shared/services/pointRemarquable/point_remarquable.service';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core/src/components/formly.field.config';
import { FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Key } from 'protractor';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';
import { ValidationService } from '../../shared/settings/validators.service';

@Component({
  selector: 'app-add-zep',
  templateUrl: './add-zep.component.html',
  styleUrls: ['./add-zep.component.scss']
})
export class AddZepComponent implements OnInit {
  settingActive : number;
  public newZep : Zep;

  public zepList: Array<Zep>;
  public zepTypeList: Array<ZepType>;
  public mesureList: Array<MesureProtection_Zep>;
  public listPostes : Array<Poste>;
  modelRef: BsModalRef;
  public onClose: Subject<Zep>;
  form = new FormGroup({});
   public listoknok = [{Id :1,Valeur:'Oui'},{Id : 2,Valeur : 'Non'}]
  fieldsInfo: FormlyFieldConfig[] = [];
  fieldsTTX: FormlyFieldConfig[] = [];
  fieldsProcedesDFV: FormlyFieldConfig[]=[]
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.newZep
    }
  };

  constructor(

    private zepService: ZepService,
    private mesuresService : MesuresService,
    private pointRemarquableService : PointRemarquableService,
    private toastSrv: PhenixToasterService
  ) {
     this.onClose = new Subject();
     this.newZep = new Zep();
     this.newZep.PointRemarquable_ZepList = new Array<PointRemarquable_Zep>()
   }

  ngOnInit() {


    Observable.forkJoin([
      this.zepService.getZepsAutorises(),
      this.zepService.getAllZepTypes(),
       this.mesuresService.GetMesuresProtectionList(),
     this.zepService.getAllPostes()
    ])
    .subscribe((response: any[]) => {
      this.zepList = response[0];
      this.zepTypeList = response[1];
       this.mesureList = response[2];
     this.listPostes = response[3];
      this.initForm();

    },
    error => {
    console.log(error);  this.toastSrv.showError("Erreur",error.error.error);
     });

  }



  initForm(){
    let now = new Date();
    let hourAfter = new Date();

    this.fieldsInfo =
    [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key:'type',
            type:'select',
            className:'row col-md-6',
            templateOptions:
            {
              label: "type zep",
              options: this.zepTypeList,
              valueProp: 'Id',
              labelProp: 'Nom',
              required: true
            }
          },
          {
            key:'image',
            type: 'file',
            className:'row col-md-6',
            templateOptions: {label: 'Image', required: false}
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DateDebutApplicabilite",
            className: 'row col-md-6',
            type: 'datetimepicker',
            defaultValue: now,
            templateOptions:{
                label: 'à partir du',
                required: true,
                type:'dateonly'
              },
              validators:
            {
            validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateFinApplicabilite',true)
            },
          },
          {
            key: "DateFinApplicabilite",
            className: 'col-md-6',
            type: 'datetimepicker',
            defaultValue: hourAfter,
            templateOptions: {
              label: 'jusqu\'au',
              required: true,
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateDebutApplicabilite',false)
            },
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:[

        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key:'type',
            type:'select',
            className:'row col-md-6',
            templateOptions:
            {
              label: "Poste",
              options: this.listPostes,
              valueProp: 'Id',
              labelProp: 'Nom',
              required: true
            }
          },
          {
            key:'type',
            type:'select',
            className:'row col-md-6',
            templateOptions:
            {
              label: "Mesure à prendre",
              options: this.mesureList,
              valueProp: 'Id',
              labelProp: 'Valeur',
              required: true
            }
          }
        ]
      }
    ]


    this.fieldsTTX =
    [

      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key:'ttx',
            type:'select',
            className:'row col-md-6',
            templateOptions:
            {
              label: "TTX Autorisés",
              options: this.listoknok,
              valueProp: 'Id',
              labelProp: 'Valeur',
              required: false
            }
          },
          {
            key:'lam',
            type:'select',
            className:'row col-md-6',
            templateOptions:
            {
              label: "LAM Autorisés",
              options: this.listoknok,
              valueProp: 'Id',
              labelProp: 'Valeur',
              required: false
            }
          }
        ]
      },
      {
        template: '<hr><h5 class="text-left">Points d\'engagement</h5>',
        className:'row col-md-12'
      },

      {
       fieldGroupClassName: "row",
       fieldGroup: [
       {
         key:'model.PointRemarquable_ZepList',
         type: "repeat",
         fieldArray: {
          className: "row",
          fieldGroup : [
          {
            key:'type',
            type:'select',
            className:'row col-md-5',
            templateOptions:
            {
              label: "Poste",
              options: this.listPostes,
              valueProp: 'Id',
              labelProp: 'Nom',
              required: true
            }
          },
          {
            key:'ptE',
            className:'row col-md-4',
            type:'input',
            templateOptions:
            {
              label:'Points d\'engagement du poste'
            }
          },
          {
            className:'row col-md-2',type: 'button',
            templateOptions: {
              label:'Ajouter',
              titre:'-',
              inputClass: 'col-md-4 btn btn-primary',
              btnClick: (event: any) => {

              },
            }

          }
        ]
       }
      }


    ]

    },
    {

      className: 'col-md-3',type: 'button', templateOptions: {
        inputClass: 'btn btn-success',
        iconClass: 'fa fa-plus',
        withLabel:false,
        titre:'',
        btnClick: (event: any) => {
       let ptsr = new PointRemarquable_Zep();
       ptsr.ZepId = this.newZep.Id;
       this.newZep.PointRemarquable_ZepList.push(ptsr);
        },
      },
    },

    ]


this.fieldsProcedesDFV =[
  {
   fieldGroupClassName: 'row',
   fieldGroup: [
     {
       key: 'DFVAvecVerificationLiberation',
       type:'checkbox',
       className: 'row col-md-11',

       templateOptions:
       {
         label: "DFV avec vérification de libération",
         required: true,
         disabled: true
       }
     }, 
      {
        key: 'DFVSansVerificationLiberationDerriereTrainOuvrant',
        type:'checkbox',
        className: 'row col-md-11',

        templateOptions:
        {
          label: "DFV sans vérification de libération derrière Train Ouvrant",
          required: true,
          disabled: true
        }
      }, 
        {
          key: 'DFVSansVerificationLiberationTTxDeclencheur',
          type:'checkbox',
          className: 'row col-md-11',
          
          templateOptions:
          {
            label: "DFV sans vérification de libération avec TTx déclencheur(s)",
            required: true,
            disabled: true
          }
        }, 
          {
            key: 'type',
            type:'checkbox',
            className: 'row col-md-11',
            
            templateOptions:
            {
              label: "DFV sans vérification de libération avec TTx stationné(s)",
              required: true,
              disabled: true
            }
          }, 
            {
              key: 'DFVRestitueeOccupee',
              type:'checkbox',
              className: 'row col-md-11',
              
              templateOptions:
              {
                label: "DFV restituée occupée",
                required: true,
                disabled: true
              }
            }, 
            {
              key: 'DFVSansVerificationLiberationTTxStationnePartieG',
              type:'checkbox',
              className: 'row col-md-11',
              
              templateOptions:
              {
                label: "DFV sans vérification de libération avec TTx stationné sur la partie G uniquement",
                required: true,
                disabled: true
              }
            }, 
            {
              key: 'GeqSansTTx',
              type:'checkbox',
              className: 'row col-md-11',
              
              templateOptions:
              {
                label: "Geq sans TTX",
                required: true,
                disabled: true
              }
            }, 
              {
                key: 'GeqAvecTTx',
                type:'checkbox',

                className: 'row col-md-11',
                

                templateOptions:
                {
                  label: "Geq avec TTx",
                  required: true,
                  disabled: true
                }
              }, 

   ]
  }
]



  }

}
