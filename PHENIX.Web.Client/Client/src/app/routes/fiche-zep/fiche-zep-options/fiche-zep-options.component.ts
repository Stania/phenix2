import { Component, OnInit, Input } from '@angular/core';
import { Zep } from '../../../shared/models/FicheZep/zep.models';

@Component({
  selector: 'fiche-zep-options',
  templateUrl: './fiche-zep-options.component.html',
  styleUrls: ['./fiche-zep-options.component.scss']
})
export class FicheZepOptionsComponent implements OnInit {
  @Input()   zep: Zep;

  constructor() { }

  ngOnInit() {
  }

}
