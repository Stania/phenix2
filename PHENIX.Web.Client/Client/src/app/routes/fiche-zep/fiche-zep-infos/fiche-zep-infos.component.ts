import { Component, OnInit, Input } from '@angular/core';

import { PopoverDirective } from 'ngx-bootstrap';
import { MesureProtection_Zep, Zep } from '../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';

@Component({
  selector: 'fiche-zep-infos',
  templateUrl: './fiche-zep-infos.component.html',
  styleUrls: ['./fiche-zep-infos.component.css']
})
export class FicheZepInfosComponent implements OnInit {

  @Input()   zep: Zep;
  @Input() listParticularite :Map<string,string[]>;

  constructor() { }

  ngOnInit() {
  }

  getConditionProtectionVerifPoste():Array<MesureProtection_Zep>{
    if(this.zep.MesureProtection_ZepList != null)
  return this.zep.MesureProtection_ZepList.filter(x => x.MesureProtectionType.Nom == Constantes_Phenix.MesureProtectionTypes.ProtectionEtVerification);
  }

  getParticularitePoste(): Array<string>{
      return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.ApplicableDansCondition)  == null ? new Array<string>() : this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.ApplicableDansCondition);
  }
  getMesureProtectionIcon(valeur : string):string{

  return valeur.includes(Constantes_Phenix.ProtectionVerificationPoste.Protection) ? 'fa fa-shield':'';
  }
  getMesureVerificationIcon(valeur : string): string{
    return valeur.includes(Constantes_Phenix.ProtectionVerificationPoste.Verification) ? 'fa fa-eye':'';
  }
}
