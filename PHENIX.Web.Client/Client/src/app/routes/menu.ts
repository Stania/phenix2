const Home = {
    text: 'Synoptique',
    link: '/synoptique/operationnel',
    icon: 'icon-vector'
};

const DfvList = {
    text: 'Liste DFV',
    link: '/dfvlist',
    icon: 'icon-vector'
}

const BiblioZeps = {
    text: 'Bibliotheque des zeps',
    link: '/zeps',
    icon: 'icon-vector'
}

const headingMain = {
    text: 'Main Navigation',
    heading: true
};

export const menu = [
    headingMain,
    Home,
    DfvList,
    BiblioZeps
];





