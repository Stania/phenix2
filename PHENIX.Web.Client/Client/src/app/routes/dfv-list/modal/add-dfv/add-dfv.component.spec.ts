/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddDfvComponent } from './add-dfv.component';

describe('AddDfvComponent', () => {
  let component: AddDfvComponent;
  let fixture: ComponentFixture<AddDfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
