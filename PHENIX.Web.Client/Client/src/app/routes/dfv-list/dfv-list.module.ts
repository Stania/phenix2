import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2TableModule } from 'ng2-table/ng2-table'
import { Route, Routes, RouterModule } from '@angular/router';
import { DfvListComponent } from './dfv-list.component';

import { SharedModule } from '../../shared/shared.module';
import { FicheDfvModule } from '../fiche-dfv/fiche-dfv.module';
import { AddDfvComponent } from './modal/add-dfv/add-dfv.component';
import { FormsModule } from '@angular/forms';

const routes : Routes = [
  {path:'', component: DfvListComponent},
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    Ng2TableModule,
    
  ],
  declarations: [DfvListComponent,AddDfvComponent],
  exports:[
    RouterModule,

  ],
  entryComponents:[
    AddDfvComponent
  ]
})
export class DfvListModule { }
