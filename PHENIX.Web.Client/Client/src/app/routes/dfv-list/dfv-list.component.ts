import { Component, OnInit } from '@angular/core';
import { DfvListService } from '../../shared/services/ficheDfv/dfv-list.service';
import { DFV } from '../../shared/models/DFV/dfv.models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { FicheDfvComponent } from '../fiche-dfv/fiche-dfv.component';
import { FicheDfvService } from '../../shared/services/ficheDfv/fiche-dfv.service';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';
import { AddDfvComponent } from './modal/add-dfv/add-dfv.component';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';
import { UtilisateurVM } from '../../shared/models/ViewModels/UtilisateurVM';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';
import { FormGroup } from '@angular/forms';


@Component({
	selector: 'dfv-list',
	templateUrl: 'dfv-list.component.html',
})
export class DfvListComponent implements OnInit {
	modelRef: BsModalRef;
	showingDFV: boolean = false;
	loading :boolean = false;
	/* When we select file */
	form = new FormGroup({});
	Name: string;
	myFile: File; /* property of File type */
	fileChange(files: any) {
		console.log(files);
		this.myFile = files[0].nativeElement;
	}
	uploadForm: any;
	filedata: any;


	constructor(private service: FicheDfvService, private modalService: BsModalService, private authSrv: AuthenticationService,
		private ficheDFVService: FicheDfvService,
		private toastSrv: PhenixToasterService) {
			this.dfvList = [];
	}

	public rows: Array<any> = [];

	public columns: Array<any> = [
		{
			title: 'Action',
			name: 'ButtonDFV',
			sort: false,
		},
		{
			title: 'N° DFV',
			name: 'Numero',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par n°' }
		},
		{
			title: 'Nuit',
			name: 'Nuit',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par nuit' }
		},
		{
			title: 'Opération N°',
			name: 'OperationNumero',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par n°' }
		},
		/*
		{
			title: 'Priorité',
			name: '',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par poste DFV' }
		},
		*/
		{
			title: 'Heure théorique de début',
			name: 'HeureTheoriqueDebut',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par heure' },
		},
		{
			title: 'Heure théorique de fin',
			name: 'HeureTheoriqueFin',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par heure' },
		},
		{
			title: 'ZEP/ Group. ZEP',
			name: 'Zep.Numero',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par zep' },
		},
		{
			title: 'Contrat travaux',
			name: 'ContratTravauxNumero',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par contrat' },
		},
		{
			title: 'Statut',
			name: 'StatusZep.Statut',
			sort: true,
			filtering: { filterString: '', placeholder: 'filtrer par statut' },
		},
	];

	public page: number = 1;
	public itemsPerPage: number = 10;
	public maxSize: number = 5;
	public numPages: number = 1;
	public length: number = 0;

	public currentUser: UtilisateurVM;

	public config: any = {
		paging: false,
		sorting: { columns: this.columns },
		filtering: { filterString: '' },
		className: ['table-striped', 'table-bordered', 'mb0', 'ng-table'] // mb0=remove margin -/- .d-table-fixed=fix column width
	};

	public dfvList: Array<any>;

	public ngOnInit(): void {
		this.loading = true;
		this.currentUser = this.authSrv.getLoggedUser();
		const result = this.ficheDFVService.getAllDFV();
		result.subscribe(
			data => {
				this.dfvList = data;
				this.dfvList.forEach(x => {
					x.ButtonDFV = "<button class='btn btn-primary btn-xs'><i class='fa fa-edit'</i></button>";
				})
				
				this.onChangeTable(this.config);
			},
			error => {
				this.toastSrv.showError("Erreur", error.error.error);
			},
		() => this.loading = false);
	}

	public changePage(page: any, data: Array<any> = this.dfvList): Array<any> {
		let start = (page.page - 1) * page.itemsPerPage;
		let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
		return data.slice(start, end);
	}

	public changeSort(data: any, config: any): any {
		if (!config.sorting) {
			return data;
		}

		let columns = this.config.sorting.columns || [];
		let columnName: string = void 0;
		let sort: string = void 0;

		for (let i = 0; i < columns.length; i++) {
			if (columns[i].sort !== '' && columns[i].sort !== false) {
				columnName = columns[i].name;
				sort = columns[i].sort;
			}
		}

		if (!columnName) {
			return data;
		}

		// simple sorting
		return data.sort((previous: any, current: any) => {
			let propertyArray: Array<string> = columnName.split('.');
			let currentPropertyValue: any;
			let previousPropertyValue: any;
			currentPropertyValue = current;
			previousPropertyValue = previous;
			propertyArray.forEach(element => {
				currentPropertyValue = currentPropertyValue[element];
				previousPropertyValue = previousPropertyValue[element];
			});
			if (previousPropertyValue > currentPropertyValue) {
				return sort === 'desc' ? -1 : 1;
			} else if (previousPropertyValue > currentPropertyValue) {
				return sort === 'asc' ? -1 : 1;
			}
			return 0;
		});
	}

	public changeFilter(data: any, config: any): any {
		let filteredData: Array<any> = data;
		this.columns.forEach((column: any) => {
			let propertyArray: Array<string> = column.name.split('.');
			let propertyValue: any;
			if (column.filtering) {
				filteredData = filteredData.filter((item: any) => {
					propertyValue = item;
					propertyArray.forEach(element => {
						propertyValue = propertyValue[element];
					});
					if (propertyValue === null || propertyValue === undefined) {
						return [];
					}
					let filteredColumn: string = propertyValue.toString();
					return filteredColumn.toUpperCase().match(column.filtering.filterString.toUpperCase());
				});
			}
		});

		if (!config.filtering) {
			return filteredData;
		}

		if (config.filtering.columnName) {
			return filteredData.filter((item: any) =>
				item[config.filtering.columnName].match(this.config.filtering.filterString));
		}

		let tempArray: Array<any> = [];

		filteredData.forEach((item: any) => {
			let flag = false;
			this.columns.forEach((column: any) => {
				let propertyArray: Array<string> = column.name.split('.');
				let propertyValue: any;
				propertyValue = item;
				propertyArray.forEach(element => {
					if (propertyValue != null)
						propertyValue = propertyValue[element];
				});

				if (propertyValue != null && propertyValue.toString().toUpperCase().match(this.config.filtering.filterString.toUpperCase())) {
					flag = true;
				}
			});
			if (flag) {
				tempArray.push(item);
			}
		});
		filteredData = tempArray;

		return filteredData;
	}

	public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
		if (config.filtering) {
			(<any>Object).assign(this.config.filtering, config.filtering);
		}

		if (config.sorting) {
			(<any>Object).assign(this.config.sorting, config.filtering);
		}
		let filteredData = this.changeFilter(this.dfvList, this.config);
		let sortedData = this.changeSort(filteredData, this.config);
		this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
		//this.length = sortedData.length;
	}

	/*
		public onCellClick(data: any): any {
			//popup pour afficher le détail de la DFV
			alert('Détail DFV');
		}
		*/

	onFileChangess(event) {
		console.log(event.target.files);
		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				this.form.get('zepImport').setValue({
					filename: file.name,
					filetype: file.type,
					value: reader.result.split(',')[1]
				})
			};
		}

	}

	onFileChange(e) {
		console.log(e.target.files);
		this.filedata = e.target.files[0];

		console.log(e);
		const uploadData = new FormData();
		var _data = {
			filename: 'File'
		}
		uploadData.append("data", JSON.stringify(_data));
		var options = { content: uploadData };
		// const result = this.importDfvService.importDfvFile(uploadData);
		// result.subscribe(
		// 	data => {
		// 		this.handleResponse(data);
		// 	},
		// 	error => {
		// 		alert("ERROR");
		// 	});
	}
	handleResponse(response: any) {
		console.log(response);
	}

	onSubmit() {
		let formdata = this.form.value;
		console.log(this.uploadForm)
		formdata.append("avatar", this.filedata);
		// const result = this.importDfvService.importDfvFile(formdata);
		// result.subscribe(
		// 	data => {
		// 		this.handleResponse(data);
		// 	},
		// 	error => {
		// 		alert("ERROR");
		// 	});
	}

	public showDFV(event: any): any {
		if (!this.showingDFV && event.column == "ButtonDFV") {
			this.showingDFV = true;
			this.ficheDFVService.getDFVByNumero(event.row.Numero).subscribe(
				data => {
					this.modelRef = this.modalService.show(FicheDfvComponent, { class: "modal-lg" });
					this.modelRef.content.dfv = data;
					this.modelRef.content.populateListe();
					this.showingDFV = false;
				},
				err => console.log(err)
			)
		}
	}

	public showDFVAfterAdd(dfv: DFV) {
		this.ficheDFVService.getDFVByNumero(dfv.Numero).subscribe(
			data => {
				this.modelRef = this.modalService.show(FicheDfvComponent, { class: "modal-lg" });
				this.modelRef.content.dfv = data;
				this.modelRef.content.populateListe();
			},
			err => console.log(err)
		)
	}

	canAddDfv(): boolean {
		if (this.currentUser != null)
			return this.currentUser.Profil == Constantes_Phenix.Profils.AgentCirculation;
		else
			return false;
	}

	public onCellClick(data: any): any {
		//popup pour afficher le détail de la DFV
		//alert('Détail DFV');
	}

	public showAddDFVModal(): any {
		this.modelRef = this.modalService.show(AddDfvComponent, { class: "modal-md" });
		(<AddDfvComponent>this.modelRef.content).onClose.subscribe(result => {
			if (result != null) {
				this.dfvList.push(result);

				this.dfvList.forEach(x => {
					x.ButtonDFV = "<button class='btn btn-primary btn-xs'><i class='fa fa-edit'</i></button>";
				})
				this.onChangeTable(this.config);
				this.showDFVAfterAdd(result);
			} else {

			}
		});
	}

}








































/*

export class DfvListComponent implements OnInit {

	dfvList: DFV[] = [];
	modelRef : BsModalRef;

	constructor(private dfvListService: DfvListService, private modalService: BsModalService, private ficheDFVService: FicheDfvService) { }

	showDFV(dfv : DFV){
		this.ficheDFVService.getDFVByNumero(dfv.Numero).subscribe(
			data => {
				this.modelRef = this.modalService.show(FicheDfvComponent, {class:"modal-lg"});
				this.modelRef.content.dfv = data;
			},
			err => console.log(err)
		)
	};

	ngOnInit() {
		this.dfvListService.getDfvList().subscribe((res) => {
			this.dfvList = res;
			console.log(res);
		});
	}
}

*/
