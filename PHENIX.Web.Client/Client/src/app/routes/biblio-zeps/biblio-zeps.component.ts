import { Component, OnInit } from '@angular/core';
import { ZepService } from '../../shared/services/ficheZep/zep.service';
import { OuiNonPipe } from '../../../app/shared/directives/pipes/boolean.pipe';
import { ColorsService } from '../../shared/colors/colors.service';
import { BibliothequeZepVM } from '../../shared/models/ViewModels/BibliothequeZepVM';
import { Zep } from '../../shared/models/FicheZep/zep.models';
import { BsModalService, BsModalRef, tr } from 'ngx-bootstrap';
import { FicheZepComponent } from '../fiche-zep/fiche-zep.component';
import { ImportZepService } from '../../shared/services/ficheZep/importZep.service';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Ng4FilesService, Ng4FilesSelected, Ng4FilesConfig, Ng4FilesStatus } from '../../shared/directives/ng4-files';
import { Router } from '@angular/router';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';
import { Result, ResultStatus } from '../../shared/models/Commun/Result';
import swal from 'sweetalert2';



@Component({
    selector: 'app-biblio-zeps',
    templateUrl: './biblio-zeps.component.html',
    styleUrls: ['./biblio-zeps.component.scss']
})

export class BiblioZepsComponent implements OnInit {
    modelRef: BsModalRef;

    /* When we select file */
    form = new FormGroup({});
    Name: string;
    myFile: File; /* property of File type */
    fileChange(files: any) {
        console.log(files);
        this.myFile = files[0].nativeElement;
    }
    uploadForm: any;
    importedFile: any;
    public selectedFiles;
    public loading = false;


    constructor(private zepService: ZepService,
        private modalService: BsModalService,
        private importZepService: ImportZepService,
        private ng4FilesService: Ng4FilesService,
        private router: Router,
        private toastSrv: PhenixToasterService) {
        //this.length = this.zepsList.length;

        this.uploadForm = new FormGroup({
            file1: new FormControl()
        });
    }

    public rows: Array<any> = [];

    public columns: Array<any> = [


        {
            title: 'Action',
            name: 'ButtonZep',
            sort: false,
        },
        // /* infos colonne "Annexe" a mettre plus tard
        // {
        //     title: 'Annexe',
        //     name: '',
        //     sort: true,
        //     filtering: { filterString: '', placeholder: 'filtrer par type' }
        // },
        // */
        {
            title: 'Type',
            name: 'Zep.ZepType.Nom',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par type' }
        },
        {
            title: 'Numéro',
            name: 'Zep.Numero',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par numero' }
        },
        {
            title: 'Date début d\'applicabilité',
            name: 'DateDebutApplicabilite',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par date' }
        },
        {
            title: 'Date fin d\'applicabilité',
            name: 'DateFinApplicabilite',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par date' }
        },
        {
            title: 'Demandes DFV',
            name: 'Zep.PosteDemandeDFV.Nom',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par poste DFV' }
        },
        {
            title: 'TTX autorisé',
            name: 'AutorisationTTX',
            sort: true,
            filtering: { filterString: '', placeholder: 'filtrer par autorisation' },
        },
    ];


    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 6;
    public numPages: number = 1;
    public length: number = 0;


    public config: any = {
        paging: false,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered', 'mb0', 'text-center', 'ng-table'] // mb0=remove margin -/- .d-table-fixed=fix column width
    };


    public zepsList: Array<BibliothequeZepVM>;

    private testConfig: Ng4FilesConfig = {
        acceptExtensions: ['xlsm'],
        // acceptExtensions: ['jpg'],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };
    public ngOnInit(): void {
        this.ng4FilesService.addConfig(this.testConfig);
        this.Displayzeps();
    }

    public Displayzeps(){
        this.loading = true;
        const result = this.zepService.getAllZep();
        result.subscribe(
            data => {
                data.forEach(element => {
                    element.DateDebutApplicabilite = new Date(element.DateDebutApplicabilite);
                    element.DateFinApplicabilite = new Date(element.DateFinApplicabilite);
                });

                this.zepsList = data.map(a => new BibliothequeZepVM(a));
               
                this.zepsList.forEach(x => {
                    x.ButtonZep = "<button class='btn btn-primary btn-xs'><i class='fa fa-edit'></i></button>";
                })

                this.length = this.zepsList.length;
                this.onChangeTable(this.config);
            },
            error => {
                this.toastSrv.showError("Erreur", error.error.error);
            },
            () => this.loading = false);
    }

    public changePage(page: any, data: Array<any> = this.zepsList): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }
        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;
        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }
        if (!columnName) {
            return data;
        }
        // simple sorting
        return data.sort((previous: any, current: any) => {
            let propertyArray: Array<string> = columnName.split('.');
            let currentPropertyValue: any;
            let previousPropertyValue: any;
            currentPropertyValue = current;
            previousPropertyValue = previous;
            propertyArray.forEach(element => {
                if (currentPropertyValue != null && previousPropertyValue != null) {
                    currentPropertyValue = currentPropertyValue[element];
                    previousPropertyValue = previousPropertyValue[element];
                }

            });
            if (previousPropertyValue > currentPropertyValue) {
                return sort === 'desc' ? -1 : 1;
            } else if (previousPropertyValue < currentPropertyValue) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            let propertyArray: Array<string> = column.name.split('.');
            let propertyValue: any;
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    propertyValue = item;
                    propertyArray.forEach(element => {
                        if (propertyValue === null || propertyValue === undefined)
                            return [];
                        propertyValue = propertyValue[element];
                    });
                    if (propertyValue === null || propertyValue === undefined) {
                        return [];
                    }

                    let filteredColumn: string = propertyValue.toString();
                    return filteredColumn.toUpperCase().match(column.filtering.filterString.toUpperCase());
                });
            }
        }
        );

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                let propertyArray: Array<string> = column.name.split('.');
                let propertyValue: any;
                propertyValue = item;
                propertyArray.forEach(element => {
                    if (propertyValue != null)
                        propertyValue = propertyValue[element];
                });
                if (propertyValue != null && propertyValue.toString().toUpperCase().match(this.config.filtering.filterString.toUpperCase())) {
                    flag = true;
                    // console.log(propertyValue.toString().toUpperCase() + ' ///  ' + this.config.filtering.filterString.toUpperCase())
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            (<any>Object).assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            (<any>Object).assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.zepsList, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        //   this.length = sortedData.length;
    }

    public showZep(event: any): any {
        if (event.column == "ButtonZep") {
            this.zepService.getZepById(event.row.Zep.Id).subscribe(
                data => {
                    this.modelRef = this.modalService.show(FicheZepComponent, { class: "modal-lg" });
                    this.modelRef.content.zep = data;
                    this.modelRef.content.PopulateListes();
                },
                err => console.log(err)
            )
        }

    }

    goSynoCreation() {
        this.router.navigateByUrl('/synoptique/creation');
    };

    onFileChangess(event) {
        console.log(event.target.files);
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.form.get('zepImport').setValue({
                    filename: file.name,
                    filetype: file.type,

                    value: reader.result.split(',')[1]
                })
            };
        }

    }

    onFileChange(e) {
        this.importedFile = e.target.files[0];
        console.log(this.importedFile + " importedFile est ici");

        const uploadData: FormData = new FormData();
        var _data = { filename: 'File' }

        uploadData.append("data", JSON.stringify(this.importedFile), this.importedFile.name);
        var options = { content: uploadData };
        
        const result = this.importZepService.importZepFile(uploadData);
        result.subscribe(
            data => {
                console.log(this);
            },
            error => {
                this.toastSrv.showError("Erreur", error.error.error);
            });
    }

    handleResponse(response: any) {
        console.log(response + "API succeeded !");
    }

    onSubmit() {
        let formdata = this.form.value;
        console.log(this.uploadForm)
        formdata.append("avatar", this.importedFile);

        const result = this.importZepService.importZepFile(formdata);
        result.subscribe(
            data => {
                this.handleResponse(data);
            },
            error => {
                this.toastSrv.showError("Erreur", error.message);
            });
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        this.loading = true;
        if (selectedFiles.status !== Ng4FilesStatus.STATUS_SUCCESS) {
            this.selectedFiles = selectedFiles.status;
            return;
        }

        if (selectedFiles.files.length > 1) {
            alert('Merci de sélectionner un seul fichier');
        }

        var file = selectedFiles.files;

        const result = this.importZepService.importZepFile(file)
        result.subscribe(
            data => {
                if(data.Status === ResultStatus.Success){
                    this.toastSrv.showSuccess("Succès","Import terminé " + data.Value + " Fiche(s) Zep importée(s). ");
                    if(data.ErrorMessage != "")
                        swal("Zeps non importées", data.ErrorMessage,"error");
                }
                this.loading = false;
                this.Displayzeps();
            },
            error => {
                if(error.status != 200){
                    this.toastSrv.showError("Erreur", error.error.error);
                    console.log(error.error.error);
                }
                this.loading = false;
            });
    }
}








