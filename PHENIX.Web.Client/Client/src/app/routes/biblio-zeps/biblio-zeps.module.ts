import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Ng2TableModule} from 'ng2-table/ng2-table'
import { Route, Routes, RouterModule } from '@angular/router';
import { BiblioZepsComponent } from './biblio-zeps.component';
import {OuiNonPipe} from '../.././shared/directives/pipes/boolean.pipe';
import { FormsModule } from '@angular/forms';
import { Ng4FilesModule } from '../../shared/directives/ng4-files';
import { SharedModule } from '../../shared/shared.module';


const routes : Routes = [
  {path:'', component: BiblioZepsComponent}
]

@NgModule({
  imports: [
    CommonModule,
    Ng2TableModule,
    RouterModule.forChild(routes),
    FormsModule,
    Ng4FilesModule,
    SharedModule,
  ],
  declarations: [
    BiblioZepsComponent,
    OuiNonPipe,
  ],
  exports:[RouterModule]
})
export class BiblioZepsModule { }
