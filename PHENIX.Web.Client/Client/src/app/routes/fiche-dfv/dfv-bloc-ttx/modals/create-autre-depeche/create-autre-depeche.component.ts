import { Component, OnInit } from '@angular/core';
import { Manoeuvre, ManoeuvreType, AutreDepeche, Depeche } from '../../../../../shared/models/DFV/dfv.models';
import { Subject } from 'rxjs';
import { PointRemarquable_Zep } from '../../../../../shared/models/FicheZep/zep.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Constantes_Phenix } from '../../../../../shared/models/Commun/constantes.model';
import { BsModalRef } from 'ngx-bootstrap';
import { AutreDepecheService } from '../../../../../shared/services/autreDepeche/autre_depeche.service';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-create-autre-depeche',
  templateUrl: './create-autre-depeche.component.html',
  styleUrls: ['./create-autre-depeche.component.scss']
})
export class CreateAutreDepecheComponent implements OnInit {

  ////INPUT
  public manoeuvre : Manoeuvre;
  public onClose: Subject<Manoeuvre>;
  public typeManoeuvre: ManoeuvreType;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
public listPointRemarquableEngage_Degage : Array<PointRemarquable_Zep>;
  ////FORM
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.manoeuvre
    }
  };

  ////PROPERTY
  public newAutreDepeche : AutreDepeche;

  constructor(public modal: BsModalRef,private autreDepecheService : AutreDepecheService,private toastSrv : PhenixToasterService) {
    this.onClose = new Subject();
    this.manoeuvre = new Manoeuvre();
    this.newAutreDepeche = new AutreDepeche();
    this.newAutreDepeche.Depeche = new Depeche();
    this.listPointRemarquableEngage_Degage = new Array<PointRemarquable_Zep>();
   }

  ngOnInit() {
  }


  initForm() {
    this.listPointRemarquableEngage_Degage = this.listPointRemarquable.filter(x =>
      x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointDegagement ||
      x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointEngagement
     )
    this.newAutreDepeche.Manoeuvre = this.manoeuvre;
    this.form = new FormGroup({});
    if (this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Engagement)
      this.initFormEngagement();
    else if (
      this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Degagement
    )
      this.initFormDegagement();
    else this.closePopup(null);
  }
  initFormEngagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.TTXNumero",
            className: "col-md-4",
            type: "input",
            defaultValue:this.manoeuvre.TTXNumero,
            templateOptions: {
              type: "text",
              disabled : true,
              label: "Numéro des TTX",
              required: true
            }
          },
          {
            key: "Manoeuvre.TTXNature",
            className: "col-md-8",
            type: "select",
            defaultValue:this.manoeuvre.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature des TTX",
              required: true,
              disabled : true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },
      {
        key: "Manoeuvre.PointRemarquable_ZepId",
        className: "col-md-8",
        type: "select",
        defaultValue:this.manoeuvre.PointRemarquable_ZepId,
        templateOptions: {
          type: "text",
          label: "Point Remarquable",
          required: true,
          options: this.listPointRemarquableEngage_Degage.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Depeche.NumeroDonne",
            className: "col-md-5",
            type: "input",

            defaultValue:0,
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro donné",
              required: true
            }
          },
          {
            key: "Depeche.NumeroRecu",
            className: "col-md-5",
            type: "input",

            defaultValue:0,
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro reçu",
              required: true
            }
          },
        ]
      },
      {
        key:"Depeche.DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        templateOptions:{
          label: 'Date et heure de la depeche',
          required: true, showTime : true
        }
      }
    ];
  }

  initFormDegagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.TTXNumero",
            className: "col-md-4",
            type: "input",

            defaultValue:this.manoeuvre.TTXNumero,
            templateOptions: {
              type: "text",
              disabled : true,
              label: "Numéro d TTx",
              required: true
            }
          },
          {
            key: "Manoeuvre.TTXNature",
            className: "col-md-8",
            type: "select",
            defaultValue:this.manoeuvre.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              disabled : true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },
      {
        key: "Manoeuvre.PointRemarquable_ZepId",
        className: "col-md-8",
        type: "select",
        defaultValue:this.manoeuvre.PointRemarquable_ZepId,
        templateOptions: {
          type: "text",
          label: "Point " + this.typeManoeuvre.Nom,
          required: true,
          options: this.listPointRemarquable.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
      {
        key: "Manoeuvre.Commentaire",
        className: "col-md-12",
        type: "textarea",
        defaultValue:this.manoeuvre.Commentaire,
        templateOptions: {
          disabled : true,
          label: "Commentaire",
          required: false,
          rows: 4,
        }
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Depeche.NumeroDonne",
            className: "col-md-6",
            type: "input",

            defaultValue:0,
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro donné",
              required: true
            }
          },
          {
            key: "Depeche.NumeroRecu",
            className: "col-md-6",
            type: "input",

            defaultValue:0,
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro reçu",
              required: true
            }
          },
        ]
      },
      {
        key:"Depeche.DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        templateOptions:{
          label: 'Date et heure de la depeche',
          required: true, showTime : true
        }
      }
    ];
  }


  submit(model :AutreDepeche ) {
    // if(model.Depeche){
    //   model.Depeche.DateDepeche.setHours(model.Depeche.DateDepeche.getHours()+2);
    // }
    var resp = this.autreDepecheService.createAutreDepecheManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès","Une autre dépêche à été créée.");
        this.closePopup(model.Manoeuvre);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
