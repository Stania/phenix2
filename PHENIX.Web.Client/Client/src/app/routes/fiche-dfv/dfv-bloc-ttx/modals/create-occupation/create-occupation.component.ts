import { Component, OnInit } from "@angular/core";
import { Occupation, OccupationType, DFV } from "../../../../../shared/models/DFV/dfv.models";
import { BsModalRef } from "ngx-bootstrap";
import { FormlyFieldConfig, FormlyFormOptions } from "@ngx-formly/core";
import { FormGroup } from "@angular/forms";
import { Constantes_Phenix } from "../../../../../shared/models/Commun/constantes.model";
import { Subject } from "rxjs/Subject";
import { OccupationService } from "../../../../../shared/services/occupation/occupation.service";
import { PhenixToasterService } from "../../../../../shared/settings/phenix-toaster.service";

@Component({
  selector: "app-create-occupation",
  templateUrl: "./create-occupation.component.html",
  styleUrls: ["./create-occupation.component.scss"]
})
export class CreateOccupationComponent implements OnInit {
  public typeOccupation: OccupationType;
  public occupation: Occupation;
  public dfv: DFV;
  public onClose: Subject<Occupation>;

  constructor(public modal: BsModalRef,private occupationService : OccupationService,private toastSrv : PhenixToasterService) {
    this.occupation = new Occupation();
    this.onClose = new Subject();

  }

  ngOnInit() {

  }

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.occupation
    }
  };

  initForm() {
    this.occupation.OccupationTypeId = this.typeOccupation.Id;
    this.occupation.DFVId = this.dfv.Id;
    this.form = new FormGroup({});
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "row col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: true
            }
          }
          ,
          {
            key: "TTXPosition",
            className: "col-md-6",
            type: "input",
            templateOptions: { type: "text", label: "Position du TTx", required: true },
            hideExpression: this.typeOccupation.Nom == Constantes_Phenix.OccupationTypes.OccupeeALAccord
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNature",
            className: "row col-md-12",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
          // {
          //   key: "TTXLieuStationnement",
          //   className: "col-md-4",
          //   type: "input",
          //   templateOptions: { type: "text", label: "Lieu de stationnement", required: true },
          //   hideExpression: this.typeOccupation.Nom == Constantes_Phenix.OccupationTypes.OccupeeALAccord
          // }
        ]
      }
    ];
  }

submit(model) {
  var resp = this.occupationService.createOccupation(model);
  resp.subscribe(a=>{
    //this.toastSrv.showSuccess("Succès","Une occupation à été créée.");
      this.closePopup(a);
    },error=>{
      console.log(error);
    });
  }

  closePopup(item : Occupation) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
