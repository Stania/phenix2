import { Component, OnInit, Input, SimpleChanges, OnChanges } from "@angular/core";
import { DFV, OccupationType, ManoeuvreType, DepecheType, AutreDepeche, Manoeuvre_Depeche } from "../../shared/models/DFV/dfv.models";
import { FicheDfvService } from "../../shared/services/ficheDfv/fiche-dfv.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { Observable } from "rxjs/Rx";
import { OccupationService } from "../../shared/services/occupation/occupation.service";
import { PointRemarquable_Zep, Poste, ZepType, Zep } from "../../shared/models/FicheZep/zep.models";
import { PointRemarquableService } from "../../shared/services/pointRemarquable/point_remarquable.service";
import { ManoeuvreService } from "../../shared/services/manoeuvre/manoeuvre.service";
import { DepechesService } from "../../shared/services/ficheDfv/depeches.service";
import { MesuresService } from "../../shared/services/ficheDfv/mesures.service";
import { ZepService } from "../../shared/services/ficheZep/zep.service";
import { AuthenticationService } from "../../shared/services/administration/authentication.service";
import { AutreDepecheService } from "../../shared/services/autreDepeche/autre_depeche.service";
import { Constantes_Phenix } from "../../shared/models/Commun/constantes.model";
import { FicheZepComponent } from "../fiche-zep/fiche-zep.component";

@Component({
  selector: "app-fiche-dfv",
  templateUrl: "./fiche-dfv.component.html",
  styleUrls: ["./fiche-dfv.component.scss"]
})
export class FicheDfvComponent implements OnInit, OnChanges {
  public dfv: DFV;
  public loading = false;
  public listTypeOccupation: Array<OccupationType>;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
  public listTypeManoeuvre: Array<ManoeuvreType>;
  public listDepecheType : Array<DepecheType>;
  public listPostes : Array<Poste>;
  public zepTypeList : Array<ZepType>;
  public zepList : Array<Zep>;
  public isPosteCreateur : boolean;
  public listAutresDepeche:Array<AutreDepeche>;
  public isAccordDisabled: boolean = true;
  public isRestitutionDisabled: boolean = true;
  public activateTabs: boolean  = false;
  public listManoeuvreDepeche:Array<Manoeuvre_Depeche>;
  modelRef: BsModalRef;
  buttonDisabled = false;

  settingActive:number;

  constructor(
    public modal: BsModalRef,
    private ficheDFVService: FicheDfvService,
    private occupationService: OccupationService,
    private pointRemarquebleService: PointRemarquableService,
    private manoeuvreService: ManoeuvreService,
    private depecheService : DepechesService,
    private mesuresService : MesuresService,
    private zepService : ZepService,
    private autreDepecheSrv : AutreDepecheService,
    private authService : AuthenticationService,
    private modalService: BsModalService

  ) {
    this.dfv = new DFV();
    this.listTypeOccupation = new Array<OccupationType>();
    this.listPointRemarquable = new Array<PointRemarquable_Zep>();
    this.listTypeManoeuvre = new Array<ManoeuvreType>();
    this.listDepecheType = new Array<DepecheType>();
    this.listAutresDepeche = new Array<AutreDepeche>();
    this.isPosteCreateur = false;
    this.listPostes = new Array<Poste>();
    this.updateTabs();
    this.listManoeuvreDepeche = new Array<Manoeuvre_Depeche>();
  }
  openTab(){
    if (this.isPosteCreateur){
      this.settingActive = 1;
    }
    else
    {
      this.settingActive = 4;
    }
  }
  updateTabs(){
    console.log(this.dfv);

    if (this.dfv && this.dfv.StatusZep){
      this.activateTabs = true;
      if (this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.EnAccord){
        this.isAccordDisabled = false;
      }else if (this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
        ||this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee){
        this.isAccordDisabled = false;
        this.isRestitutionDisabled = false;
      }
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    let model = changes.dfv;
    this.populateListe();
  }

  onDfvChange(dfv : DFV){
    this.dfv = dfv;
    this.updateTabs();
  }

  ngOnInit() {
    this.openTab();

    this.updateTabs();
  }

  populateListe(){
    this.loading = true;
    var stop = "stop";
    let loggedUser = this.authService.getLoggedUser();
    if  (this.dfv !== null && this.dfv.ResponsableDFV !== null  && this.dfv.ResponsableDFV.Nom == loggedUser.Poste){
      this.isPosteCreateur = true;
    }
    else{
      this.isPosteCreateur = false;
    }
    this.openTab();

    Observable.forkJoin([
      this.occupationService.getAllOccupationType(),
      this.manoeuvreService.getAllManoeuvreType(),
      this.pointRemarquebleService.GetListPointRemarquableByIdZep(this.dfv.ZepId),
      this.depecheService.getAllDepecheType(),
      this.zepService.getAllPostes(),
      this.autreDepecheSrv.getAllAutreDepecheByIdDFV(this.dfv.Id),
      this.depecheService.getListManoeuvreDepecheByIdDfv(this.dfv.Id),
      this.zepService.getAllZepTypes(),
      this.zepService.getZepsAutorises()
    ]).subscribe((response: any[]) => {
      this.listTypeOccupation = response[0];
      this.listTypeManoeuvre = response[1];
      this.listPointRemarquable = response[2];
      this.listDepecheType = response[3];
      this.listPostes = response[4];
      this.listAutresDepeche = response[5]
      this.listManoeuvreDepeche = response[6];
      this.zepTypeList = response[7];
      this.zepList = response[8];
      this.updateTabs();
      this.loading = false;

    });
  }

  refreshListDepeche(){
    let resDepeche =  this.depecheService.getListManoeuvreDepecheByIdDfv(this.dfv.Id)
    resDepeche.subscribe(list => {
      this.listManoeuvreDepeche = list;
    });
  }

  refreshListAutreDepeche(param : any){
    let resAutreDepeche = this.autreDepecheSrv.getAllAutreDepecheByIdDFV(this.dfv.Id)
    resAutreDepeche.subscribe(list => {
      this.listAutresDepeche = list;
    });
  }

  closePopup() {
    this.modal.hide();
    this.modal =null;
  }

  isBlocDemandeDisabled():boolean{
    return !(this.dfv && this.dfv.StatusZep && this.dfv.StatusZep.Statut === "En création");
  }

  submit() {
    let model = this.dfv;
    this.ficheDFVService.validerDemande(this.dfv).subscribe(
      data => {
        this.dfv = data;
      },
      err => {
        console.log(err);
        alert("erreur");
      }
    )
  }

  openZep(){
    this.zepService.getZepById(this.dfv.ZepId).subscribe(
      data => {
        this.modelRef = this.modalService.show(FicheZepComponent, {class:"modal-lg"});
        this.modelRef.content.zep = data;
        this.modelRef.content.PopulateListes();
      },
      err => console.log(err)
    )
  }

}
