import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DFV, OccupationType, ManoeuvreType, DepecheType } from '../../../shared/models/DFV/dfv.models';
import { PointRemarquable_Zep } from '../../../shared/models/FicheZep/zep.models';

@Component({
  selector: 'app-dfv-bloc-notification',
  templateUrl: './dfv-bloc-notification.component.html',
  styleUrls: ['./dfv-bloc-notification.component.scss']
})
export class DfvBlocNotificationComponent implements OnInit, OnChanges {

  @Input() dfv: DFV;
  @Output() dfvChange = new EventEmitter<DFV>();
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() listDepecheType : Array<DepecheType>;
  @Input() isPosteCreateur : boolean;
  @Input() isNotification : boolean = false;

  ngOnChanges(changes: SimpleChanges): void {

  }

  onDfvChange(dfv : DFV){
    this.dfv = dfv;
    this.dfvChange.emit(this.dfv);
  }

  constructor() {
    this.dfv = new DFV();

  }

  ngOnInit() {
  }

}
