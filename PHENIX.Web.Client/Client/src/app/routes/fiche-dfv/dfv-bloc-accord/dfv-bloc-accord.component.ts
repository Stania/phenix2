import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import {
  DFV,
  ManoeuvreType,
  Manoeuvre,
  OccupationType,
  Occupation
} from "../../../shared/models/DFV/dfv.models";
import {
  PointRemarquable_Zep,
  Zep,
  ZepType
} from "../../../shared/models/FicheZep/zep.models";
import { Constantes_Phenix } from "../../../shared/models/Commun/constantes.model";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { ManoeuvreService } from "../../../shared/services/manoeuvre/manoeuvre.service";
import { PhenixToasterService } from "../../../shared/settings/phenix-toaster.service";
import { FormlyFormOptions, FormlyFieldConfig } from "@ngx-formly/core";
import { FormGroup } from "@angular/forms";
import { ValidationService } from "../../../shared/settings/validators.service";
import { ZepService } from "../../../shared/services/ficheZep/zep.service";
import { Observable } from "rxjs/Rx";
import { OccupationService } from "../../../shared/services/occupation/occupation.service";
import * as _ from "lodash";
import { FicheDfvService } from "../../../shared/services/ficheDfv/fiche-dfv.service";
import Swal from 'sweetalert2';

@Component({
  selector: "app-dfv-bloc-accord",
  templateUrl: "./dfv-bloc-accord.component.html",
  styleUrls: ["./dfv-bloc-accord.component.scss"]
})
export class DfvBlocAccordComponent implements OnInit {
  @Input() dfv: DFV;
  @Input() listTypeManoeuvre: Array<ManoeuvreType>;
  @Input() listTypeOccupation: Array<OccupationType>;
  @Input() zepTypeList : Array<ZepType>;
  @Input() zepList : Array<Zep>;
  @Input() isPosteCreateur : boolean;
  @Output() dfvChange = new EventEmitter<DFV>();

  modelRef: BsModalRef;
  listFranchissement: Array<Manoeuvre>;
  dfvCopy: DFV;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
      model: this.dfv
    }
  };

  constructor(
    private zepService: ZepService,
    private modalService: BsModalService,
    private manoeuvreService: ManoeuvreService,
    private toastSrv: PhenixToasterService,
    private occupationSrv: OccupationService,
    private dfvSrv: FicheDfvService
  ) {
    this.dfv = new DFV();
    this.dfvCopy = new DFV();
    this.listTypeManoeuvre = new Array<ManoeuvreType>();
    this.listTypeOccupation = new Array<OccupationType>();
    this.zepTypeList = new Array<ZepType>();
    this.zepList = new Array<Zep>();
  }

  ngOnInit() {
    // Observable.forkJoin([
    //   this.zepService.getAllZep(),
    //   this.zepService.getAllZepTypes()
    // ]).subscribe((response: any[]) => {
    //   this.zepList = response[0];
    //   this.zepTypeList = response[1];
    //   this.initForm();
    // });
  }

  initForm() {

    this.form = new FormGroup({});
    this.dfvCopy = _.cloneDeep(this.dfv);
    this.dfvCopy.ManoeuvreList = this.manoeuvreService.filtrerManoeuvre(
      Constantes_Phenix.ManoeuvreTypes.Franchissement,
      this.dfv.ManoeuvreList,
      this.listTypeManoeuvre
    );
    this.dfvCopy.OccupationList = this.occupationSrv.filtrerOccupations(
      Constantes_Phenix.OccupationTypes.OccupeeALAccord,
      this.dfv.OccupationList
    );

    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Numero",
            className: "col-md-4",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro DFV",
              required: true,
              disabled: true
            }
          },
          {
            key: "Zep.ZepTypeId",
            className: "col-md-4",
            type: "select",
            templateOptions: {
              label: "Type Zep",
              options: this.zepTypeList,
              valueProp: "Id",
              labelProp: "Nom",
              required: true,
              disabled: true
            }
          },
          {
            key: "ZepId",
            type: "select",
            className: "col-md-4",
            templateOptions: {
              label: "Numéro Zep",
              options: this.zepList,
              valueProp: "Id",
              labelProp: "Numero",
              required: true,
              disabled: true
            }
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "AvecVerificationDeLiberation",
            type: "checkbox",
            className: "col-md-6",

            templateOptions: {
              label: "Avec vérification de libération",
              required: true,
              disabled: true
            }
          }
        ]
      },
      {
        className: "section-label",
        hideExpression: "!model.DerriereTrainOuvrant",
        template:
          "<hr /><div class='col-md-12'><strong>Elément d'identification du train ouvrant :</strong></div>"
      },
      {
        key: "DerriereTrainOuvrantNumero",
        className: "col-md-12",
        type: "input",
        templateOptions: {
          type: "text",
          label: "Derrière train ouvrant n° :",
          required: true,
          disabled: this.isDFVAccorde()
        },
        hideExpression: "!model.DerriereTrainOuvrant"
      },

      {
        className: "section-label",
        template:
          "<hr /><div class='col-md-12'><strong>Lieu de stationnement de matériel roulant :</strong></div>"
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "OccupationList",
            type: "repeat",
            fieldArray: {
              className: "row",
              fieldGroup: [
                {
                  key: "TTXNumero",
                  className: "col-md-4",
                  type: "input",
                  templateOptions: {
                    type: "text",
                    label: "Numéro du TTx",
                    required: true,
                    disabled: true
                  }
                },
                {
                  key: "TTXLieuStationnement",
                  className: "col-md-7",
                  type: "input",
                  templateOptions: {
                    type: "text",
                    label: "Lieu de stationnement du matériel roulant",
                    required: true,
                    disabled: this.isDFVAccorde()
                  }
                }
              ]
            }
          }
        ]
      },
      {
        className: "section-label",
        template:
          "<hr /><div class='col-md-12'><strong>Autorisation de franchissement des signaux intermédiaires :</strong></div>"
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "ManoeuvreList",
            type: "repeat",
            fieldArray: {
              className: "row",
              fieldGroup: [
                {
                  key: "PointRemarquable_Zep.Valeur",
                  className: "col-md-5",
                  type: "input",
                  templateOptions: {
                    type: "text",
                    label: "Signal intermédiaire",
                    disabled: true
                  }
                },
                {
                  key: "Commentaire",
                  className: "col-md-4",
                  type: "input",
                  templateOptions: {
                    type: "text",
                    label: "Commentaire",
                    disabled: true
                  }
                },
                {
                  key: "FranchissementEffectue",
                  type: "checkbox",
                  className: "col-md-3",
                  defaultValue: false,
                  templateOptions: {
                    for: "Id",
                    align: true,
                    disabled: this.isDFVAccorde(),
                    label: "Effectué ?"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "DateDebutAccord",
            className: "col-md-6",
            type: "datetimepicker",
            defaultValue: this.dfv.DateDebutAccord == null ? new Date(this.dfv.DateDebutApplicabilite): new Date(this.dfv.DateDebutAccord),
            templateOptions: {
              label: "Date de début d'accord",
              required: true,
              disabled:this.isDFVAccorde(), showTime : true
            },
            validators: {
              validation: x =>
                ValidationService.periodValidator(
                  x,
                  this.form,
                  "DateFinAccord",
                  true
                )
            }
          },
          {
            key: "DateFinAccord",
            className: "col-md-6",
            type: "datetimepicker",
            defaultValue: this.dfv.DateFinAccord == null ? new Date(this.dfv.DateFinApplicabilite): new Date(this.dfv.DateFinAccord),
            templateOptions: { label: "Date de fin d'accord", required: true,disabled:this.isDFVAccorde(), showTime : true },
            validators: {
              validation: x =>
                ValidationService.periodValidator(
                  x,
                  this.form,
                  "DateDebutAccord",
                  false
                )
            }
          }
        ]
      },
      {
        key: "AccordeParEcrit",
        type: "radio",
        className: "col-md-12",
        defaultValue: true,
        templateOptions: {
          label: "Accorde ?",
          required: true,
          disabled:this.isDFVAccorde(),
          options: [
            {
              value: "Accord Par écrit",
              key: true
            },
            {
              value: "Accord Par dépêche",
              key: false
            }
          ]
        }
      },
      {
        key: "TransmisParDepecheNumero",
        className: "col-md-4",
        type: "input",
        templateOptions: {
          type: "text",
          label: "Transmis par dépêche n° :",
          required: true,
          disabled:this.isDFVAccorde()
        },
        hideExpression:
          "(model.AccordeParEcrit == true || model.AccordeParEcrit == null)"
      }
    ];
    this.options.formState.model = this.dfvCopy;
  }

  isDFVAccorde(): boolean {
    return (
      this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
    );
  }

  applyDisable(){
    // apply expressionProperty for disabled based on formState to all fields
    this.fields.forEach(field => {
     field.expressionProperties = field.expressionProperties || {};
     field.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
     if(field.fieldGroup){
       field.fieldGroup.forEach(t => {
         t.expressionProperties = t.expressionProperties || {};
         t.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
       });
     }
   });
 }

  submit(model: DFV) {
    var resp = this.dfvSrv.accordeDFV(model);
    resp.subscribe(
      a => {
        this.dfv = a;
        this.dfvChange.emit(a);
        this.applyDisable();
        this.toastSrv.showSuccess(
          "Succès",
          "La DFV a été accordée."
        );
        this.initForm();

      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }
}
