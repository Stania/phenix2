import { Component, OnInit, Input, OnChanges, SimpleChanges, Output } from '@angular/core';
import { DepecheType, Depeche, Manoeuvre } from '../../../shared/models/DFV/dfv.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { DepechesService } from '../../../shared/services/ficheDfv/depeches.service';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';

@Component({
  selector: 'app-depeche',
  templateUrl: './depeche.component.html',
  styleUrls: ['./depeche.component.scss']
})
export class DepecheComponent implements OnInit, OnChanges {

  @Input() depecheType : DepecheType;
  @Input() mesureId : number;
  @Input() manoeuvreId : number;

  depeche : Depeche;
  public onClose: Subject<Depeche>;

  form : FormGroup;
  fields:FormlyFieldConfig[];
  options : FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
      model: this.depeche,
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initForm();
  }

  constructor(private depechesService : DepechesService,  public modalRef: BsModalRef) {
    this.form = new FormGroup({});
    this.depeche = new Depeche();
    this.onClose = new Subject();
    this.initForm();
  }

  initForm(){

    this.fields = [
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
              key:"NumeroDonne",
              className:"col-md-6",
              type:"input",
              templateOptions:{
                type:"text",
                label: 'Numéro donné',
                required: true,
              }
          },
          {
            key:"NumeroRecu",
            className:"col-md-6",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro reçu',
              required: true,
            }
          }
        ]
      },
      {
        key:"DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        defaultValue : new Date(this.depeche.DateDepeche),
        templateOptions:{
          label: 'Date et heure de la dépêche',
          required: true, showTime : true
        }
      }
    ]
  }

  ngOnInit() {

  }

  public closePopup(){
    this.modalRef.hide();
    this.onClose.next(this.depeche);
  }

  submit() {
    this.depeche.DepecheTypeId = this.depecheType.Id;
    var response = null;
    if(this.depecheType.Nom == Constantes_Phenix.DepecheType.MesureProtection){
      response = this.depechesService.AddDepeche(this.depeche, this.mesureId);
    }else if(this.depecheType.Nom == Constantes_Phenix.DepecheType.NotificationEngagement
      || this.depecheType.Nom == Constantes_Phenix.DepecheType.RestitutionEngagement){
      response = this.depechesService.AddDepecheManoeuvre(this.depeche, this.manoeuvreId);
    }else if(this.depecheType.Nom == Constantes_Phenix.DepecheType.NotificationDegagement
      || this.depecheType.Nom == Constantes_Phenix.DepecheType.RestitutionDegagement){
      response = this.depechesService.AddDepecheManoeuvre(this.depeche, this.manoeuvreId);
    }
    else{
      response = this.depechesService.AddDepeche(this.depeche, this.mesureId);
    }

    response.subscribe(
      data => {
        this.depeche = data;
        this.closePopup();
      },
      err => {
        alert("erreur");
        console.log(err);
      }
    );

  }

}
