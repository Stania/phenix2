import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FormlyFieldConfig, FormlyFormOptions } from "@ngx-formly/core";
import {
  Manoeuvre,
  DFV,
  Depeche,
  Manoeuvre_Depeche
} from "../../../../../shared/models/DFV/dfv.models";
import { BsModalRef } from "ngx-bootstrap";
import { ManoeuvreService } from "../../../../../shared/services/manoeuvre/manoeuvre.service";
import { PhenixToasterService } from "../../../../../shared/settings/phenix-toaster.service";
import { Subject } from "rxjs";
import { PointRemarquable_Zep } from "../../../../../shared/models/FicheZep/zep.models";
import { Constantes_Phenix } from "../../../../../shared/models/Commun/constantes.model";
import {EffectueManoeuvreViewModel} from "../../../../../shared/models/ViewModels/EffectueManoeuvreViewModel.model";

@Component({
  selector: "app-effectuer-engagement",
  templateUrl: "./effectuer-engagement.component.html",
  styleUrls: ["./effectuer-engagement.component.scss"]
})
export class EffectuerEngagementComponent implements OnInit {
  public engagement: Manoeuvre;
  public dfv: DFV;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
  public onClose: Subject<Manoeuvre>;
public effectueManoeuvreModel : EffectueManoeuvreViewModel;
  public depeche: Depeche;
  ////FORM
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.engagement
    }
  };

  constructor(
    public modal: BsModalRef,
    private manoeuvreService: ManoeuvreService,
    private toastSrv: PhenixToasterService
  ) {
    this.engagement = new Manoeuvre();
    this.onClose = new Subject();
    this.depeche = new Depeche();
    this.effectueManoeuvreModel = new EffectueManoeuvreViewModel();
  }

  ngOnInit() {}
  initForm() {

this.effectueManoeuvreModel.DepecheToAdd = this.depeche;
this.effectueManoeuvreModel.Manoeuvre = this.engagement;

    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.TTXNumero",
            className: "col-md-4",
            type: "input",
            defaultValue: this.effectueManoeuvreModel.Manoeuvre.TTXNumero,
            templateOptions: {
              type: "text",
              disabled: true,
              label: "Numéro des TTX",
              required: true
            }
          },
          {
            key: "Manoeuvre.TTXNature",
            className: "col-md-8",
            type: "select",
            defaultValue: this.effectueManoeuvreModel.Manoeuvre.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature des TTX",
              required: true,
              disabled: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              }))
            }
          }
        ]
      },
      {
        key: "Manoeuvre.PointRemarquable_ZepId",
        className: "col-md-12",
        type: "select",
        defaultValue: this.effectueManoeuvreModel.Manoeuvre.PointRemarquable_ZepId,
        templateOptions: {
          type: "text",
          label: "Point Remarquable",
          required: true,
          disabled: true,
          options: this.listPointRemarquable.map(x => ({
            label: x.PosteConcerne.Nom + " / " + x.Valeur,
            value: x.Id
          }))
        }
      },
      {
        className: "section-label",
        template:
          "<hr /><div class='col-md-12'><strong>Autorisation :</strong></div>"
      },
      {
        key: "Manoeuvre.autorisationCheck",
        type: "multicheckbox",
        className: "col-md-12",
        templateOptions: {
          checkClick: (selectedOption: { key; value }, event: any) => {
            let optionsKey = ["Verbale", "ParEcrit", "ParDepeche"];
            this.effectueManoeuvreModel.Manoeuvre[selectedOption.key] = true;
            let tempOptionKeyArray = optionsKey.filter(
              x => x != selectedOption.key
            );
            this.effectueManoeuvreModel.Manoeuvre.SignatureRptx = false;
            let object = {};
            object[selectedOption.key] = true;
            tempOptionKeyArray.forEach(x => {
              this.effectueManoeuvreModel.Manoeuvre[x] = false;
              object[x] = false;
            });
            let autorisationField = this.form.get("Manoeuvre.autorisationCheck");
            if (autorisationField != null) autorisationField.setValue(object);
          },
          options: [
            { key: "Verbale", value: "Verbable" },
            { key: "ParEcrit", value: "Par écrit" },
            { key: "ParDepeche", value: "Par dépêche" }
          ],required:true,
        }
      },

      {
        key: "DepecheToAdd.NumeroDonne",
        className: "col-md-4 col-md-offset-2",
        type: "input",

        templateOptions: {
          type: "text",
          disabled: false,
          label: "Numéro donné",
          required: true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },

      {
        key: "DepecheToAdd.NumeroRecu",
        className: "col-md-4",
        type: "input",
        templateOptions: {
          type: "text",
          disabled: false,
          label: "Numéro reçu",
          required: true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },
      {
        key: "DepecheToAdd.DateDepeche",
        className: "col-md-12",
        defaultValue: new Date(this.effectueManoeuvreModel.DepecheToAdd.DateDepeche),
        type: "datetimepicker",
        templateOptions: {
          label: "Date et heure de la depeche",
          required: true, showTime : true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },
      {
        key: "Manoeuvre.SignatureRptx",
        className: "col-md-12",
        type: "checkbox",
        templateOptions: {
          label: "Signature RPTX",
          required: true
        },
        hideExpression: "!model.Manoeuvre.ParEcrit"
      },
    ];
  }

  submit(model: EffectueManoeuvreViewModel) {
    // if(model.Manoeuvre.ParDepeche){
    //   model.DepecheToAdd.DateDepeche.setHours(model.DepecheToAdd.DateDepeche.getHours()+2);
    // }
    var resp = this.manoeuvreService.effectueManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès", "La manoeuvre à été effectuée.");
        this.closePopup(a);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
