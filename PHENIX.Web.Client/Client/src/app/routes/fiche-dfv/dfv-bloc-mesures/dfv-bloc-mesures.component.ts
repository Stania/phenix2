import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DFV, MesureProtectionPrise, DepecheType } from '../../../shared/models/DFV/dfv.models';
import { MesureProtection_Zep, Poste } from '../../../shared/models/FicheZep/zep.models';
import { MomentModule } from 'angular2-moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { DepecheComponent } from '../depeche/depeche.component';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { CreateMesureComponent } from './modals/create-mesure/create-mesure.component';
import Swal from 'sweetalert2';
import { MesuresService } from '../../../shared/services/ficheDfv/mesures.service';
import { isDate } from 'moment';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import * as moment from 'moment'

@Component({
  selector: 'app-dfv-bloc-mesures',
  templateUrl: './dfv-bloc-mesures.component.html',
  styleUrls: ['./dfv-bloc-mesures.component.scss']
})
export class DfvBlocMesuresComponent implements OnInit, OnChanges {

  @Input() dfv : DFV;
  @Input() listDepecheType : Array<DepecheType>;
  @Input() isPosteCreateur : boolean;

  modalRef : BsModalRef;
  modalMesure : BsModalRef;
  postes : Poste[];
  isAccordee : boolean = false;

  constructor(
    private modalService : BsModalService,
    private ficheDfvService : FicheDfvService,
    private mesuresService : MesuresService,
    private authService :AuthenticationService
  )
  {
    this.dfv = new DFV();
    this.listDepecheType = new Array<DepecheType>();
    this.postes = new Array<Poste>();
    this.isMesuresParAC = false;
  }

  isMesuresParAC : boolean;
  formMesure = new FormGroup({});
  fieldsMesure: FormlyFieldConfig[] = new Array<FormlyFieldConfig>();
  optionsFormMesure: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfv,
    }
  };
  initFormMesure(){
    this.formMesure = new FormGroup({});
    this.dfv.MesureProtectionPiseParAC = true;
    this.fieldsMesure =  [
      {

        fieldGroupClassName:"row",
        fieldGroup:[
          // {
          //   key:"MesureProtectionPiseParAC",
          //   className:"col-md-12",
          //   type:'checkbox',
          //   templateOptions:{
          //     label: 'Mesures de protection prises par l\'AC',
          //     required: true,
          //     disabled: this.isMesuresParAC
          //   }
          // },
          {
            key:"DateMesureProtectionPiseParAC",
            className:"col-md-12",
            type:"datetimepicker",
            defaultValue : this.dfv.DateMesureProtectionPiseParAC == null ?  new Date()  : new Date(this.dfv.DateMesureProtectionPiseParAC),
            hideExpression:'model.MesureProtectionPiseParAC != true',
            templateOptions:{
              label: '',
              required: this.dfv.MesureProtectionPiseParAC,
              disabled: this.isMesuresParAC || this.isAccordee, showTime : true
            }
          },
        ]
      }

    ]
    this.optionsFormMesure.formState.model = this.dfv;
  }

  ngOnChanges(changes: SimpleChanges): void {
      let model = this.dfv;
  }

  ngOnInit() {
    if (this.dfv.MesureProtectionPiseParAC === true) this.isMesuresParAC = true;
    if (this.dfv != null && this.dfv.StatusZep != null){
      if(this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
        || this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee){
        this.isAccordee = true;
      }
    }

    this.initFormMesure();

  }

  getRowSpan(mesure : MesureProtectionPrise){
      if(this.getPropDepeche(mesure,"Date") != ""){
        return  3;
      }else{
        return  1;
      }
  }

  getDateFormat(date){
    if(isDate(date)){
      return date;
    }else{
      return null;
    }
  }

  PasDeDepeche(mesure : MesureProtectionPrise){
    let yapas = this.getPropDepeche(mesure,"Date") === "";
    return yapas;
  }

  getPropDepeche(mesure : MesureProtectionPrise, typeNumero : string){
    var depeches = mesure.MesureProtectionPrise_DepecheList;

    if(depeches && depeches.length > 0){
      let md =  depeches.find(t => t.Depeche.DepecheType.Nom == "Mesure de protection");
      if(md){
        console.log(md);
        switch(typeNumero){
          case "Donne":
            return md.Depeche.NumeroDonne;
          case "Recu":
            return md.Depeche.NumeroRecu;
          case "Date":
            return md.Depeche.DateDepeche;
        }
      }else{
        return  "";
      }
    }else{
      return "";
    }
  }

  addDepeche(mesure : MesureProtectionPrise){
    this.modalRef = this.modalService.show(DepecheComponent);
    this.modalRef.content.depecheType = this.listDepecheType.find(t => t.Nom == "Mesure de protection");
    this.modalRef.content.mesureId = mesure.Id;

    this.modalRef.content.onClose.subscribe(result => {
      if (result != null) {
        this.ficheDfvService.getDFVByNumero(this.dfv.Numero).subscribe(
          data => {
            this.dfv = data;
          },
          err => {
            console.log(err);
            alert("erreur");
          }
        )
      } else {
        alert("erreur");
      }
    });
  }

  addMesure(){
    this.postes = this.dfv.Zep.MesureProtection_ZepList.map(t => t.Poste);
    let posteDemandes = this.dfv.MesureProtectionPriseList.map(t => t.PosteDemande);
    this.postes = this.postes.filter(t => posteDemandes.filter((x,i) => x.Nom === t.Nom).length < 1);
    if(this.postes.length<1){
      // alert("Tous les postes prévus dans la Zep ont été ajoutés");
      Swal("Tous les postes prévus dans la Zep ont été ajoutés!");
      return;
    }

    let mesure = new MesureProtectionPrise();
    mesure.DFVId = this.dfv.Id;
    this.modalMesure = this.modalService.show(CreateMesureComponent);
    this.modalMesure.content.add = true;
    this.modalMesure.content.mesure = mesure;
    this.modalMesure.content.postes = this.postes;
    this.modalMesure.content.initForm();
    this.modalMesure.content.onClose.subscribe(data => {
      if(data != null){
        this.dfv.MesureProtectionPriseList.push(data);
      }
    });
  }

  editMesure(mesure : MesureProtectionPrise ){
    this.postes = this.dfv.Zep.MesureProtection_ZepList.map(t => t.Poste);
    this.modalMesure = this.modalService.show(CreateMesureComponent);
    this.modalMesure.content.add = false;
    this.modalMesure.content.mesure = mesure;
    this.modalMesure.content.postes = this.postes;
    this.modalMesure.content.initForm();
    this.modalMesure.content.onClose.subscribe(data => {
      if(data != null){
        var edited = this.dfv.MesureProtectionPriseList.find(t => t.Id == data.Id);
        edited = data;
      }
    });
  }

  deleteMesure(mesure : MesureProtectionPrise){
    this.mesuresService.DeleteMesure(mesure).subscribe(
      data => {
        this.dfv.MesureProtectionPriseList =
        this.dfv.MesureProtectionPriseList.filter(t => t.Id != mesure.Id);
        Swal('Suppression',"Mesure supprimée", "success");
      },
      err => {
        console.log(err);
        alert("erreur");
      }
    )
  }

  submitMesure(){
    let stop = "stop";
    // this.dfv.DateMesureProtectionPiseParAC.setHours(this.dfv.DateMesureProtectionPiseParAC.getHours()+2);
    this.ficheDfvService.UpdateDfv(this.dfv).subscribe(
      data => {
        this.dfv = data;
        Swal('Mise à jour !', "La modification à été enregistrée", 'success');
        if (this.dfv.MesureProtectionPiseParAC === true) this.isMesuresParAC = true;
        this.initFormMesure();
      },
      err => {
        alert("erreur");
        console.log(err);
      }
    )
  }

  filterMesures() : Array<MesureProtectionPrise>{
    if (this.dfv.MesureProtectionPriseList == null){
      return new Array<MesureProtectionPrise>();;
    }
    if (this.isPosteCreateur){
      return this.dfv.MesureProtectionPriseList
    }
    else{
      return this.dfv.MesureProtectionPriseList.filter(a=>a.PosteDemande.Nom === this.authService.getLoggedUser().Poste);
    }
  }


}
