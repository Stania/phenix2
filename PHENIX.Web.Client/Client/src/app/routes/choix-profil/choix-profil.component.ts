import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfil, Profil, Secteur } from '../../shared/models/Profils/profil.models';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';
import { Router } from '@angular/router';
import { Poste } from '../../shared/models/FicheZep/zep.models';
import { UtilisateurService } from '../../shared/services/administration/utilisateur.service';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-choix-profil',
  templateUrl: './choix-profil.component.html',
  styleUrls: ['./choix-profil.component.scss']
})
export class ChoixProfilComponent implements OnInit {

    valForm: FormGroup;
    choixProfil : UserProfil;
    listChoixProfil : Array<UserProfil>;
    constructor(fb: FormBuilder,
       private userService : UtilisateurService,
       private authService : AuthenticationService,
       private router : Router,
       private toastSrv : PhenixToasterService
      ) {

        this.valForm = fb.group({
            'choixprofil': [null, Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
          this.choixProfil = new UserProfil();
          this.choixProfil = this.valForm.value['choixprofil'];
          console.log(this.choixProfil);

          var resp = this.authService.choixProfil(this.choixProfil).subscribe(a=>{
            if (a === true ){
              this.router.navigate([this.authService.getRedirectUrlAfterChoix()])
            }
            else{

            }
          },error=>{
            console.log(error);
          });


        }
    }

    ngOnInit() {
      const result =  this.userService.getChoixProfilList();
      result.subscribe(
        data => {
          this.listChoixProfil = data;
       },
       error => {
        this.toastSrv.showError("Erreur",error.error.error);
       });

    //   this.listChoixProfil = new Array<UserProfil>();
    //   this.listChoixProfil.push(<UserProfil>({
    //     Id : 1,
    //     ProfilId : 1,
    //     SecteurId : 1,
    //     PosteId : 1,
    //     Profil : <Profil>({Id : 1, Nom : 'AC'}),
    //     Secteur : <Secteur>({Id : 1, Nom : 'Mantes'}),
    //     Poste : <Poste>({Id : 1, Nom : 'PRCI'}),
    //  }))
    //  this.listChoixProfil.push(<UserProfil>({
    //       Id : 1,
    //       ProfilId : 1,
    //       SecteurId : 1,
    //       PosteId : 2,
    //       Profil : <Profil>({Id : 1, Nom : 'AC'}),
    //       Secteur : <Secteur>({Id : 1, Nom : 'Mantes'}),
    //       Poste : <Poste>({Id : 2, Nom : 'PRS'}),
    //   }))
    }

}
