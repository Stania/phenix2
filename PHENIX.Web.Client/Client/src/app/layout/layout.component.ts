import { Component, OnInit } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster/src/toaster-config';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  public toasterConfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-right'
  });

    constructor() { }

    ngOnInit() {
    }

}
