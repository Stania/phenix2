﻿using Clic.BLL.IServices;
using Clic.BLL.Services.FicheZep;
using Clic.DAL.Entities.FicheZep;
using Clic.Data.DbContext;
using CLic.BLL.Services.FicheZep;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PHNIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Clic.DAL.Persistance.Commun;
using System.Linq;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.Profils;
using System.IO;

namespace CLIC.TESTS.ImportZep
{
    [TestClass]
    public class ImportZepTest
    {
        private IImportZepService _ImportZepService;
        private IZepService _ZepService;

        public ImportZepTest()
        {
            _ZepService = new ZepService();
            _ImportZepService = new ImportZepService(_ZepService);
        }

        [TestMethod]
        public void ImportZepExcel()
        {
            string pathBase = AppDomain.CurrentDomain.BaseDirectory;
            pathBase = pathBase.Split("\\bin").FirstOrDefault();
            string path = pathBase + "\\Fichier Import ZEP V7.xlsm";
            string res = _ImportZepService.ImportFichesZepsExcel(path);
        }
    }
}
