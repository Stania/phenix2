﻿
using Clic.BLL.IServices;
using Clic.BLL.IServices.Profil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Clic.BLL.Services.Profil
{
    public class PasswordHasher : IPasswordHasher
    { 

        private readonly int _iterations;

        public PasswordHasher(int iterations = 25000)
        {
            _iterations = iterations;
        }

        /// <summary>
        /// Hash password with salt and return string to store in the format {Iterations}.{Salt}{Hash}
        /// </summary>
        /// <param name="password">The clear text password to hash</param>
        /// <returns>A string including the iterations, salt and hash to be stored in the database</returns>
        public string HashPassword(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            var saltAndHash = Crypto.HashPassword(password, _iterations);
            return string.Format("{0}.{1}", _iterations, saltAndHash);
        }



        /// <summary>
        /// Verify that the hash of the supplied password matches the stored hash for that user.
        /// </summary>
        /// <param name="hashedPassword">The hash stored in the database in the format {Iterations}.{Salt}{Hash}</param>
        /// <param name="providedPassword">The clear text password to verify</param>
        /// <returns>A PasswordVerificationResult. If the iterations have changed since the user last logged in SuccessRehashNeeded is returned.</returns>
        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (hashedPassword == null)
            {
                throw new ArgumentNullException("hashedPassword");
            }
            if (providedPassword == null)
            {
                throw new ArgumentNullException("providedPassword");
            }

            var parts = hashedPassword.Split('.');
            int iterations;
            if (parts.Length != 2 || !int.TryParse(parts[0], out iterations))
            {
                return PasswordVerificationResult.Failed;
            }
            var hashAndSalt = parts[1];
            bool isEqual = Crypto.VerifyHashedPassword(hashAndSalt, providedPassword, iterations);
            if (!isEqual)
            {
                return PasswordVerificationResult.Failed;
            }
            return _iterations == iterations
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.SuccessRehashNeeded;
        }
    }
}
