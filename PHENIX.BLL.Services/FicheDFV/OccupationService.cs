﻿using Clic.DAL.Entities.FicheDFV;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Linq;
using Clic.DAL.Persistance.ExtentionMethods;
using System.Collections.Generic;

namespace Clic.BLL.Services.FicheDFV
{
    public class OccupationService : IOccupationService
    {
        public Occupation GetOccupationById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.Occupation.Any(t => t.Id == Id))
                    throw new ArgumentException("Argument invalide : " + nameof(Id));
                return context.Occupation
                    .Include(t => t.OccupationType)
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public Occupation AddOccupation(Occupation occupation)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<Occupation>(occupation,true);
                var newOccupation = context.Occupation.Add(occupation).Entity;
                context.SaveChanges();
                return this.GetOccupationById(newOccupation.Id) ;
            }
        }

        public Occupation UpdateOccupation(Occupation occupation)
        {
            using (AppDbContext context = new AppDbContext())
            {
                occupation.DFV = null;
                occupation.OccupationType = null;
                context.ValidateEntity<Occupation>(occupation);
                context.Occupation.Attach(occupation);
                context.Entry(occupation).State = EntityState.Modified;
                context.SaveChanges();
                return this.GetOccupationById(occupation.Id);
            }
        }

        public void DeleteOccupation(Occupation occupation)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<Occupation>(occupation);
                Occupation occ = GetOccupationById(occupation.Id);
                context.Occupation.Remove(occ);
                context.SaveChanges();
            }
        }
        public OccupationType GetOccupationTypeByName(string nom)
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.OccupationType.FirstOrDefault(t => t.Nom == nom);
            }
        }
        public List<OccupationType> GetAllOccupationType()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.OccupationType.ToList();
            }
        }
    }
}
