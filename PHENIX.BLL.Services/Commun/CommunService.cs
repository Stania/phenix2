﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Clic.Data.DbContext;
using PHENIX.BLL.IServices;
using System.Collections.Generic;
using System.Linq;

namespace Clic.BLL.Services.Commun
{
    public class CommunService : ICommunService
    {
        public Dictionary<string, List<string>> GetConfiguration()
        {
            Dictionary<string, List<string>> dico = new Dictionary<string, List<string>>();
            using(AppDbContext context = new AppDbContext())
            {
                List<ZepType> zepTypes = context.ZepType.ToList();
                List<OccupationType> occupationTypes = context.OccupationType.ToList();
                List<PointRemarquableType> PRTypes = context.PointRemarquableType.ToList();
                List<MesureProtectionType> mesuresTypes = context.MesureProtectionType.ToList();
                List<ManoeuvreType> manoeuvreTypes = context.ManoeuvreType.ToList();
                List<DepecheType> depecheTypes = context.DepecheType.ToList();

            }
            return dico;
        }
    }
}
