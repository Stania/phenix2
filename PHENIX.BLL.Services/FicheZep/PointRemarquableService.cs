﻿using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Linq;
using Clic.DAL.Persistance.ExtentionMethods;
using System.Collections.Generic;
using Clic.DAL.Entities.FicheZep;

namespace Clic.BLL.Services.FicheZep
{
    public class PointRemarquableService : IPointRemarquableService
    {
        public List<PointRemarquable_Zep> GetListPointRemarquableByIdZep(int idZep)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.Zep.Any(t => t.Id == idZep))
                    throw new ArgumentException("Argument invalide : " + nameof(idZep));

                return context.PointRemarquable_Zep
                    .Include(t => t.Zep)
                    .Include(t => t.PosteConcerne)
                    .Include(t => t.PointRemarquableType)
                    .Where(t => t.ZepId == idZep)
                    .ToList();
            }
        }
        public PointRemarquable_Zep GetPointRemarquable_ZepById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.PointRemarquable_Zep.Any(t => t.Id == Id))
                    throw new ArgumentException("Argument invalide : " + nameof(Id));

                return context.PointRemarquable_Zep
                    .Include(t => t.PosteConcerne)
                    .Include(t => t.PointRemarquableType)
                    .Include(t => t.Zep)
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }
    }
}
