﻿using CLic.BLL.Services.FicheZep;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Clic.BLL.IServices;
using System.Text;
using Newtonsoft.Json.Serialization;
using Clic.BLL.IServices.Profil;
using Clic.BLL.Services.Profil;
using PHENIX.BLL.IServices;
using Clic.BLL.Services.FicheDFV;
using PHNIX.BLL.IServices;
using Clic.BLL.Services.FicheZep;
using Clic.Web.API.Common;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace Clic.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // When used with ASP.net core, add these lines to Startup.cs
            //var connectionString = Configuration.GetConnectionString("PHENIXContext");
            //services.AddEntityFrameworkNpgsql().AddDbContext<AppDbContext>(options => options.UseNpgsql(connectionString));
            var SigningKey = "jKQwDwKcutNWVimCiS1hM6GF7cdkr2T-exG_FuY41yg";
            //Configuration["SigningKey"]
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "PHENIX_NG",//Configuration["Issuer"],
                    ValidAudience = "51f783b666a52ec79a70b268e9a256Gg",//Configuration["Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SigningKey))
                };
            });
           
            services.AddCors();
            services.AddMvc()
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling =Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
               //options.SerializerSettings.DateFormatString = "dd/MM/yyyy";
            });

            // Injection de dépendance
            services.AddSingleton<IZepService, ZepService>();
            services.AddSingleton<IDFVService, DFVService>();
            services.AddSingleton<IOccupationService, OccupationService>();
            services.AddSingleton<IUtilisateurService, UtilisateurService>();
            services.AddSingleton<IImportZepService, ImportZepService>();
            services.AddSingleton<IManoeuvreService, ManoeuvreService>();
            services.AddSingleton<IPointRemarquableService, PointRemarquableService>();
            services.AddSingleton<IMesureService, MesureService>();
            services.AddSingleton<IAutreDepecheService, AutreDepecheService>();
            services.AddSingleton<IDepecheService, DepecheService>();

            services.AddCors();
            //services.AddCors(options => options.AddPolicy("AllowAllOrigin", builder => {
            //    builder.AllowAnyOrigin();
            //    builder.AllowAnyHeader();
            //    builder.AllowAnyMethod();
            //}));
            //services.Configure<MvcOptions>(options =>
            //    options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAllOrigin")
            //));
            services.AddMemoryCache();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin()
            );
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseMvc();
            app.UseMvcWithDefaultRoute();

            //app.UseExceptionHandler(
            // options => {
            //     options.Run(
            //     async context =>
            //     {
            //         context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            //         context.Response.ContentType = "text/html";
            //         var ex = context.Features.Get<IExceptionHandlerFeature>();
            //         if (ex != null)
            //         {
            //             var err = ex.Error.Message;
            //             await context.Response.WriteAsync(err).ConfigureAwait(false);
            //         }
            //     });
            // }
            //);

        }
    }
}
