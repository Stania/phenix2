﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clic.DAL.Entities.FicheDFV;
using Clic.Web.API.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/Occupation")]
    public class OccupationController : Controller
    {
        private IOccupationService _OccupationService;
        private IMemoryCache _cache;

        public OccupationController(IOccupationService occupationService,
            IMemoryCache memoryCache)
        {
            _OccupationService = occupationService;
            _cache = memoryCache;
        }
       
        // POST api/<controller>
        [HttpPost]
        [Route("create")]
        public Occupation Create([FromBody]Occupation occupation)
        {
           return  _OccupationService.AddOccupation(occupation);
        }

        // POST api/<controller>
        [HttpPost]
        [Route("update")]
        public Occupation Update([FromBody]Occupation occupation)
        {
           return _OccupationService.UpdateOccupation(occupation);
        }

        // DELETE api/<controller>/5
        [HttpPost]
        [Route("DeleteOccupation/{id}")]
        public Object Delete(int id)
        {
            var occup = _OccupationService.GetOccupationById(id);
            
            try
            {
                _OccupationService.DeleteOccupation(occup);
                return new Object(); 
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllOccupationType")]
        public List<OccupationType> GetAllOccupationType()
        {
            string key = CacheKeys.OccupationTypeList;
            List<OccupationType> typeOccupationTypeList = new List<OccupationType>();
            if (!_cache.TryGetValue<List<OccupationType>>(key, out typeOccupationTypeList))
            {
                typeOccupationTypeList = _OccupationService.GetAllOccupationType();
                _cache.Set<List<OccupationType>>(key, typeOccupationTypeList);
            }
            return typeOccupationTypeList;
        }
    }
}
