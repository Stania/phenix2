﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Clic.BLL.IServices.Profil;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using Clic.DAL.Entities.Profils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace PHENIX.Web.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class IdentityController : Controller
    {
        private IUtilisateurService UtilisateurService { get; set; }
        private string SigningKey { get; set; } = "jKQwDwKcutNWVimCiS1hM6GF7cdkr2T-exG_FuY41yg";
        private string Issuer { get; set; } = "PHENIX_NG";
        private string Audience { get; set; } = "51f783b666a52ec79a70b268e9a256Gg";

        public IdentityController(IUtilisateurService pUtilisateurService)
        {
            UtilisateurService = pUtilisateurService;
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody]LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                //This method returns user id from username and password.
                //identity service
                Utilisateur user = this.UtilisateurService.Authenticate(loginViewModel.Login, loginViewModel.Password);
                if (user == null)
                {
                    return Unauthorized();
                }

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, loginViewModel.Login),
                };

                var token = new JwtSecurityToken
                (
                    issuer: Issuer,
                    audience: Audience,
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(60),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SigningKey)),
                            SecurityAlgorithms.HmacSha256)

                );


                return Ok(new
                {
                    access_token = new JwtSecurityTokenHandler().WriteToken(token),
                    Login = loginViewModel.Login,
                    Poste = "",
                    Profil = "",
                    Secteur = ""
                });
            }

            return BadRequest();
        }


        [HttpPost]
        [Route("ChoixProfil")]
        public IActionResult ChoixProfil([FromBody]UserProfil pChoix)
        {

            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string userName = User.Identity.Name;
                //This method returns user id from username and password.
                //identity service
                bool isUserInRole = this.UtilisateurService.IsInRole(pChoix);
                if (!isUserInRole)
                {
                    return Unauthorized();
                }

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, userName),
                    new Claim(Constantes.UserClaims.Profil, pChoix.Profil.Nom),
                    new Claim(Constantes.UserClaims.Secteur, pChoix.Secteur.Nom),
                    new Claim(Constantes.UserClaims.Poste, pChoix.Poste.Nom)
                };

                var token = new JwtSecurityToken
                (
                    issuer: Issuer,
                    audience: Audience,
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(60),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SigningKey)),
                            SecurityAlgorithms.HmacSha256)

                );


                return Ok(new
                {
                    access_token = new JwtSecurityTokenHandler().WriteToken(token),
                    Login = userName,
                    Poste = pChoix.Poste.Nom,
                    Profil = pChoix.Profil.Nom,
                    Secteur = pChoix.Secteur.Nom
                });
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("GetChoixProfilList")]
        public ICollection<UserProfil> GetChoixProfilList()
        {
            string userName = User.Identity.Name;
            return this.UtilisateurService.GetUserProfils(userName);
        }

        [Route("Register")]
        [HttpPost]
        public IActionResult Regsiter([FromBody]AddUtilisateurModel utilisateur)
        {
            Utilisateur user = this.UtilisateurService.CreateUser(utilisateur.Utilisateur, utilisateur.Password);
            return Ok(user);
        }
    }

    public class LoginViewModel
    {
        public string ClientID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class AddUtilisateurModel
    {
        public Utilisateur Utilisateur { get; set; }
        public string Password { get; set; }
        public Profil Profil { get; set; }
        public Poste Poste { get; set; }
        public Secteur Secteur { get; set; }

    }


}