﻿using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.ViewModels
{
    public class ListZepViewModel
    {
        List<ZepViewModel> ListeZep { get; set; }
    }
}
