﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface ICommunService
    {
        /// <summary>
        /// Récupère l'ensemble des listes à utiliser dans les listes déroulantes
        /// </summary>
        /// <returns></returns>
        Dictionary<string, List<string>> GetConfiguration();
            
    }
}
