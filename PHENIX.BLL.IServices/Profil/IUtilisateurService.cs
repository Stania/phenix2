﻿using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.BLL.IServices.Profil
{
    public interface IUtilisateurService
    {
        Utilisateur Authenticate(string pLogin, string pPassword);
        Utilisateur CreateUser(Utilisateur pUtilisateur, string pPassword);
        Utilisateur GetUser(string pUsername);
        Utilisateur UpdateUser(Utilisateur pUtilisateur);
        Utilisateur UpdateUser(Utilisateur pUtilisateur, string pPassword);
        ICollection<UserProfil> GetUserProfils(string pUserName);
        Boolean IsInRole(UserProfil pUserProfil);
        Clic.DAL.Entities.Profil.Profil GetProfilByName(String pProfilName);
        
    }
}
