﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class MesureProtectionPriseConfiguration : IEntityTypeConfiguration<MesureProtectionPrise>
    {
        void IEntityTypeConfiguration<MesureProtectionPrise>.Configure(EntityTypeBuilder<MesureProtectionPrise> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.DFV)
                .WithMany(t => t.MesureProtectionPriseList)
                .HasForeignKey(t => t.DFVId)
                .IsRequired();

            builder.HasOne(t => t.PosteDemande)
                .WithMany(t => t.MesureProtectionPriseList)
                .HasForeignKey(t => t.PosteDemandeId);


        }
    }
}
