﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class DepecheTypeConfiguration : IEntityTypeConfiguration<DepecheType>
    {
        void IEntityTypeConfiguration<DepecheType>.Configure(EntityTypeBuilder<DepecheType> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(t => t.Nom)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}
