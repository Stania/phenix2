﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class AutreDepecheTypeConfiguration : IEntityTypeConfiguration<AutreDepecheType>
    {
        void IEntityTypeConfiguration<AutreDepecheType>.Configure(EntityTypeBuilder<AutreDepecheType> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(t => t.Nom)
                .HasMaxLength(50)
                .IsRequired();
            builder.Property(t => t.Propriete)
               .HasMaxLength(50)
               .IsRequired();
        }
    }
}
