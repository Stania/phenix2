﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class OccupationTypeConfiguration : IEntityTypeConfiguration<OccupationType>
    {
        void IEntityTypeConfiguration<OccupationType>.Configure(EntityTypeBuilder<OccupationType> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
