﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class DepecheConfiguration : IEntityTypeConfiguration<Depeche>
    {
        void IEntityTypeConfiguration<Depeche>.Configure(EntityTypeBuilder<Depeche> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.NumeroDonne)
               .IsRequired()
               .HasMaxLength(50);

            builder.HasOne(p => p.DepecheType)
                .WithMany(t => t.DepecheList)
                .HasForeignKey(t => t.DepecheTypeId)
                .IsRequired();
        }
    }
}
