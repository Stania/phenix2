﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class AccordDFV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateFinAccordee",
                table: "DFV",
                newName: "DateFinAccord");

            migrationBuilder.RenameColumn(
                name: "DateDebutAccordee",
                table: "DFV",
                newName: "DateDebutAccord");

            migrationBuilder.AddColumn<string>(
                name: "TTXLieuStationnement",
                table: "Occupation",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "AvecVerificationDeLiberation",
                table: "DFV",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TTXLieuStationnement",
                table: "Occupation");

            migrationBuilder.RenameColumn(
                name: "DateFinAccord",
                table: "DFV",
                newName: "DateFinAccordee");

            migrationBuilder.RenameColumn(
                name: "DateDebutAccord",
                table: "DFV",
                newName: "DateDebutAccordee");

            migrationBuilder.AlterColumn<bool>(
                name: "AvecVerificationDeLiberation",
                table: "DFV",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
