﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class manoeuvre_null_autreDepeche : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AutreDepeche_Manoeuvre_ManoeuvreId",
                table: "AutreDepeche");

            migrationBuilder.AlterColumn<int>(
                name: "ManoeuvreId",
                table: "AutreDepeche",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_AutreDepeche_Manoeuvre_ManoeuvreId",
                table: "AutreDepeche",
                column: "ManoeuvreId",
                principalTable: "Manoeuvre",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AutreDepeche_Manoeuvre_ManoeuvreId",
                table: "AutreDepeche");

            migrationBuilder.AlterColumn<int>(
                name: "ManoeuvreId",
                table: "AutreDepeche",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AutreDepeche_Manoeuvre_ManoeuvreId",
                table: "AutreDepeche",
                column: "ManoeuvreId",
                principalTable: "Manoeuvre",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
