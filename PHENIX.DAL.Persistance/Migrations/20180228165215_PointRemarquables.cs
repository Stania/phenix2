﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class PointRemarquables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_PointRemarquableId",
                table: "Manoeuvre");

            migrationBuilder.DropForeignKey(
                name: "FK_PointRemarquable_Zep_PointRemarquable_PointRemarquableId",
                table: "PointRemarquable_Zep");

            migrationBuilder.DropTable(
                name: "PointRemarquable");

            migrationBuilder.RenameColumn(
                name: "PointRemarquableId",
                table: "PointRemarquable_Zep",
                newName: "PointRemarquableTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_PointRemarquable_Zep_PointRemarquableId",
                table: "PointRemarquable_Zep",
                newName: "IX_PointRemarquable_Zep_PointRemarquableTypeId");

            migrationBuilder.RenameColumn(
                name: "PointRemarquableId",
                table: "Manoeuvre",
                newName: "PointRemarquable_ZepId");

            migrationBuilder.RenameIndex(
                name: "IX_Manoeuvre_PointRemarquableId",
                table: "Manoeuvre",
                newName: "IX_Manoeuvre_PointRemarquable_ZepId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre",
                column: "PointRemarquable_ZepId",
                principalTable: "PointRemarquable_Zep",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PointRemarquable_Zep_PointRemarquableType_PointRemarquableTypeId",
                table: "PointRemarquable_Zep",
                column: "PointRemarquableTypeId",
                principalTable: "PointRemarquableType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre");

            migrationBuilder.DropForeignKey(
                name: "FK_PointRemarquable_Zep_PointRemarquableType_PointRemarquableTypeId",
                table: "PointRemarquable_Zep");

            migrationBuilder.RenameColumn(
                name: "PointRemarquableTypeId",
                table: "PointRemarquable_Zep",
                newName: "PointRemarquableId");

            migrationBuilder.RenameIndex(
                name: "IX_PointRemarquable_Zep_PointRemarquableTypeId",
                table: "PointRemarquable_Zep",
                newName: "IX_PointRemarquable_Zep_PointRemarquableId");

            migrationBuilder.RenameColumn(
                name: "PointRemarquable_ZepId",
                table: "Manoeuvre",
                newName: "PointRemarquableId");

            migrationBuilder.RenameIndex(
                name: "IX_Manoeuvre_PointRemarquable_ZepId",
                table: "Manoeuvre",
                newName: "IX_Manoeuvre_PointRemarquableId");

            migrationBuilder.CreateTable(
                name: "PointRemarquable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true),
                    PointRemarquableTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PointRemarquable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PointRemarquable_PointRemarquableType_PointRemarquableTypeId",
                        column: x => x.PointRemarquableTypeId,
                        principalTable: "PointRemarquableType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PointRemarquable_PointRemarquableTypeId",
                table: "PointRemarquable",
                column: "PointRemarquableTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_PointRemarquableId",
                table: "Manoeuvre",
                column: "PointRemarquableId",
                principalTable: "PointRemarquable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PointRemarquable_Zep_PointRemarquable_PointRemarquableId",
                table: "PointRemarquable_Zep",
                column: "PointRemarquableId",
                principalTable: "PointRemarquable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
