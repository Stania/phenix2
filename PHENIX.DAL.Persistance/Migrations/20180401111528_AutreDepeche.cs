﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class AutreDepeche : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AutreDepecheType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AutreDepecheType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AutreDepeche",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AncienneValeur = table.Column<string>(nullable: true),
                    AutreDepecheTypeId = table.Column<int>(nullable: false),
                    DepecheId = table.Column<int>(nullable: false),
                    ManoeuvreId = table.Column<int>(nullable: false),
                    NouvelleValeur = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AutreDepeche", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AutreDepeche_AutreDepecheType_AutreDepecheTypeId",
                        column: x => x.AutreDepecheTypeId,
                        principalTable: "AutreDepecheType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AutreDepeche_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AutreDepeche_Manoeuvre_ManoeuvreId",
                        column: x => x.ManoeuvreId,
                        principalTable: "Manoeuvre",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AutreDepeche_AutreDepecheTypeId",
                table: "AutreDepeche",
                column: "AutreDepecheTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AutreDepeche_DepecheId",
                table: "AutreDepeche",
                column: "DepecheId");

            migrationBuilder.CreateIndex(
                name: "IX_AutreDepeche_ManoeuvreId",
                table: "AutreDepeche",
                column: "ManoeuvreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AutreDepeche");

            migrationBuilder.DropTable(
                name: "AutreDepecheType");
        }
    }
}
