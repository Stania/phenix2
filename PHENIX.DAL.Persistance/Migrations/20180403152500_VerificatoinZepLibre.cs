﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class VerificatoinZepLibre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DestinationTrain",
                table: "VerificationZepLibre_Ligne",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrigineTrain",
                table: "VerificationZepLibre_Ligne",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DestinationTrain",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropColumn(
                name: "OrigineTrain",
                table: "VerificationZepLibre_Ligne");
        }
    }
}
