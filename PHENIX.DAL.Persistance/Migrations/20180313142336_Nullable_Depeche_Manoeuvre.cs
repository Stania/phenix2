﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Nullable_Depeche_Manoeuvre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.AlterColumn<int>(
                name: "DepecheId",
                table: "Manoeuvre",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.AlterColumn<int>(
                name: "DepecheId",
                table: "Manoeuvre",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
