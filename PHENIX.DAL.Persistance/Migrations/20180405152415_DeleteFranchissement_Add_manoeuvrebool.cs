﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class DeleteFranchissement_Add_manoeuvrebool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Franchissement");

            migrationBuilder.AddColumn<bool>(
                name: "FranchissementEffectue",
                table: "Manoeuvre",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FranchissementEffectue",
                table: "Manoeuvre");

            migrationBuilder.CreateTable(
                name: "Franchissement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchissement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Franchissement_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Franchissement_DFVId",
                table: "Franchissement",
                column: "DFVId");
        }
    }
}
