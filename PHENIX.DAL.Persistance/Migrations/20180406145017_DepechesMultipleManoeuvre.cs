﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class DepechesMultipleManoeuvre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.DropIndex(
                name: "IX_Manoeuvre_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "DepecheId",
                table: "Manoeuvre");

            migrationBuilder.AddColumn<int>(
                name: "ManoeuvreId",
                table: "Depeche",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Manoeuvre_Depeche",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DepecheId = table.Column<int>(nullable: false),
                    ManoeuvreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manoeuvre_Depeche", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Manoeuvre_Depeche_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Manoeuvre_Depeche_Manoeuvre_ManoeuvreId",
                        column: x => x.ManoeuvreId,
                        principalTable: "Manoeuvre",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Depeche_ManoeuvreId",
                table: "Depeche",
                column: "ManoeuvreId");

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre_Depeche",
                column: "DepecheId");

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_Depeche_ManoeuvreId",
                table: "Manoeuvre_Depeche",
                column: "ManoeuvreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Depeche_Manoeuvre_ManoeuvreId",
                table: "Depeche",
                column: "ManoeuvreId",
                principalTable: "Manoeuvre",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Depeche_Manoeuvre_ManoeuvreId",
                table: "Depeche");

            migrationBuilder.DropTable(
                name: "Manoeuvre_Depeche");

            migrationBuilder.DropIndex(
                name: "IX_Depeche_ManoeuvreId",
                table: "Depeche");

            migrationBuilder.DropColumn(
                name: "ManoeuvreId",
                table: "Depeche");

            migrationBuilder.AddColumn<int>(
                name: "DepecheId",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
