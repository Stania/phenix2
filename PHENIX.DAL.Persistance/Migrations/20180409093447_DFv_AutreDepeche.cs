﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class DFv_AutreDepeche : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProprieteManoeuvre",
                table: "AutreDepecheType",
                newName: "Propriete");

            migrationBuilder.AddColumn<int>(
                name: "DFVId",
                table: "AutreDepeche",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AutreDepeche_DFVId",
                table: "AutreDepeche",
                column: "DFVId");

            migrationBuilder.AddForeignKey(
                name: "FK_AutreDepeche_DFV_DFVId",
                table: "AutreDepeche",
                column: "DFVId",
                principalTable: "DFV",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AutreDepeche_DFV_DFVId",
                table: "AutreDepeche");

            migrationBuilder.DropIndex(
                name: "IX_AutreDepeche_DFVId",
                table: "AutreDepeche");

            migrationBuilder.DropColumn(
                name: "DFVId",
                table: "AutreDepeche");

            migrationBuilder.RenameColumn(
                name: "Propriete",
                table: "AutreDepecheType",
                newName: "ProprieteManoeuvre");
        }
    }
}
