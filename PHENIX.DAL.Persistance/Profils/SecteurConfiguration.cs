﻿using Clic.DAL.Entities.Profil;
using Clic.DAL.Entities.Profils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.Profils
{
    public class SecteurConfiguration : IEntityTypeConfiguration<Secteur>
    {
        void IEntityTypeConfiguration<Secteur>.Configure(EntityTypeBuilder<Secteur> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
