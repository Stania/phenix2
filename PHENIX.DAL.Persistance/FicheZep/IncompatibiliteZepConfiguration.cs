﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class IncompatibiliteZepConfiguration : IEntityTypeConfiguration<IncompatibiliteZep>
    {
        void IEntityTypeConfiguration<IncompatibiliteZep>.Configure(EntityTypeBuilder<IncompatibiliteZep> builder)
        {
            builder.HasKey(e => new { e.ZepIncompatibleId, e.ZepId });
        }
    }
}
