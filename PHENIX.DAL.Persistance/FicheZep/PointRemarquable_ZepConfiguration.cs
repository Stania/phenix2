﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class PointRemarquable_ZepConfiguration : IEntityTypeConfiguration<PointRemarquable_Zep>
    {
        void IEntityTypeConfiguration<PointRemarquable_Zep>.Configure(EntityTypeBuilder<PointRemarquable_Zep> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Zep)
                .WithMany(t => t.PointRemarquable_ZepList)
                .HasForeignKey(p => p.ZepId);

           
            builder.HasOne(p => p.PosteConcerne)
                .WithMany(t => t.PointRemarquable_ZepList)
                .HasForeignKey(p => p.PosteConcerneId);

            builder.HasOne(p => p.PointRemarquableType)
                .WithMany(p => p.PointRemarquable_ZepList)
                .HasForeignKey(p => p.PointRemarquableTypeId);

        }
    }
}
