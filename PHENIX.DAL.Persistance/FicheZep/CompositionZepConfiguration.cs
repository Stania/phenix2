﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class CompositionZepConfiguration : IEntityTypeConfiguration<CompositionZep>
    {
        void IEntityTypeConfiguration<CompositionZep>.Configure(EntityTypeBuilder<CompositionZep> builder)
        {
            builder.HasKey(e => new { e.GroupeZepId, e.ZepEnfantId });
        }
    }
}
