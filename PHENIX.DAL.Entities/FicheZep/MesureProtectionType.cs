﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class MesureProtectionType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<MesureProtection_Zep> MesureProtection_ZepList { get; set; }
    }
}
