﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class IncompatibiliteZep: EntityBase
    {
        public int ZepId { get; set; }
        [ForeignKey(nameof(ZepId))]
        public Zep Zep { get; set; }

        public int ZepIncompatibleId { get; set; }
        [ForeignKey(nameof(ZepIncompatibleId))]
        public Zep ZepIncompatible { get; set; }

    }
}
