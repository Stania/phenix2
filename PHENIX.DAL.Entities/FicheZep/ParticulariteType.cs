﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class ParticulariteType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Particularite> ParticulariteList { get; set; }
    }
}
