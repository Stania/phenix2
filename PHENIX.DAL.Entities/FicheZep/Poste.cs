﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.Profil;
using Clic.DAL.Entities.Profils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class Poste : EntityBase
    {
        public string Nom { get; set; }

        public int SecteurId { get; set; }
        public Secteur Secteur { get; set; }

        public ICollection<Zep> ZepList { get; set; }

        public ICollection<MesureProtection_Zep> MesureProtection_ZepList { get; set; }
        public ICollection<PointRemarquable_Zep> PointRemarquable_ZepList { get; set; }
        public ICollection<FicheDFV.DFV> DFVList { get; set; }

        public ICollection<MesureProtectionPrise> MesureProtectionPriseList { get; set; }

        public VerificationZepLibre_IPCS VerificationZepLibre_IPCS { get; set; }
        public VerificationZepLibre_Ligne VerificationZepLibre_Ligne { get; set; }
        public ICollection<Verification> VerificationList { get; set; }

        public ICollection<UserProfil> UserProfilList { get; set; }
        public ICollection<ProfilPoste> ProfilPosteList { get; set; }


    }
}
