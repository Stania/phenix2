﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class Aiguille : EntityBase
    {
        public string Nom { get; set; }
        public int ZepId { get; set; }
        public Zep Zep { get; set; }
        public ICollection<Aiguille_DFV> Aiguille_DFVList { get; set; }
    }
}
