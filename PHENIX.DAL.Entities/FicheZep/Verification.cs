﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class Verification : EntityBase
    {
        public string Nom { get; set; }

        public int PosteId { get; set; }
        public Poste Poste { get; set; }

        public int ZepId { get; set; }
        public Zep Zep { get; set; }

        public int VerificationTypeId { get; set; }
        public VerificationType VerificationType { get; set; }
    }
}
