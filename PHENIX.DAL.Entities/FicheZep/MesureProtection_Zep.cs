﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class MesureProtection_Zep : EntityBase
    {
        public string Valeur { get; set; }

        public int ZepId { get; set; }
        public int PosteId { get; set; }

        public Zep Zep { get; set; }
        public Poste Poste { get; set; }

        public int MesureProtectionTypeId { get; set; }
        public MesureProtectionType MesureProtectionType { get; set; }

    }
}
