﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class ZepType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Zep> ZepList { get; set; }
    }

    
}
