﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Profil
{
    public class Utilisateur : EntityBase
    {
        public string Nom { get; set; }
        public string Login { get; set; }
        public string HasshedPassword { get; set; }
        public ICollection<UserProfil> UserProfilList { get; set; }

    }
}
