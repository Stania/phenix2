﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Profil
{
    public class Profil : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<UserProfil> UserProfilList { get; set; }
        public ICollection<ProfilPoste> ProfilPosteList { get; set; }
    }
}
