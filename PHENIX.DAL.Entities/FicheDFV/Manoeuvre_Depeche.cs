﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
 
    public class Manoeuvre_Depeche : EntityBase
    {
        public Depeche Depeche { get; set; }
        public int DepecheId { get; set; }
        public int ManoeuvreId { get; set; }
        public Manoeuvre Manoeuvre { get; set; }
    }
}
