﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class Depeche : EntityBase
    {
        public string NumeroDonne { get; set; }
        public string NumeroRecu { get; set; }
        public DateTime DateDepeche { get; set; }
        
        public int DepecheTypeId { get; set; }
        public DepecheType DepecheType { get; set; }

        public VerificationZepLibre_IPCS VerificationZepLibre_IPCS { get; set; }
        public Manoeuvre Manoeuvre { get; set; }

        [InverseProperty(nameof(VerificationZepLibre_Ligne.Depeche))]
        public ICollection<VerificationZepLibre_Ligne> VerificationZepLibre_LigneList { get; set; }
        [InverseProperty(nameof(VerificationZepLibre_Ligne.Depeche2))]
        public ICollection<VerificationZepLibre_Ligne> VerificationZepLibre2_LigneList { get; set; }

        public ICollection<MesureProtectionPrise_Depeche> MesureProtectionPrise_DepecheList { get; set; }
        public ICollection<AutreDepeche> AutreDepecheList { get; set; }
        public ICollection<Manoeuvre_Depeche> Manoeuvre_DepecheList { get; set; }

        protected override void OnValidateData()
        {
            if (string.IsNullOrWhiteSpace(this.NumeroDonne) || string.IsNullOrWhiteSpace(this.NumeroRecu) || this.DateDepeche == null)
            {
                throw new ArgumentException("Une dépêche doit avoir deux numéro (donné et reçu) et une date-heure : ");
            }
            
            if (this.DepecheTypeId < 1)
            {
                throw new ArgumentException("Une dépêche doit avoir un type dépêche.");
            }
        }
    }
}
